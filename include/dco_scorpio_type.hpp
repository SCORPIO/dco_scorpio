#ifndef  __DCOS_TYPE__
#define  __DCOS_TYPE__

#include "dco_scorpio.hpp"

#define smax std::numeric_limits<std::streamsize>::max()

namespace dco {
  namespace ia1s {
  class type {
  private:
    double _value;
    long _key;
    std::string _op;
    long _pred1, _pred2;
    static long _cnt, _cntops;
    static bool _lasgn;
    static std::string _section;

  public:
    ///////////////
    // constuctors
    ///////////////
    type() : _value(0.0), _key(_cnt++), _op("C"), _pred1(-1), _pred2(-1) {
      DBG1(" dco_flattener :: default constructor");
      vec_nodes.push_back(node(_section,_key,_op,_value,-1,-1));
    }

    type( double v ) : _value(v), _key(_cnt++), _op("C"), _pred1(-1), _pred2(-1) {
      DBG1(" dco_flattener :: double constructor");
      vec_nodes.push_back(node(_section,_key,_op,_value,-1,-1));
    }

    // binary constructor
    type( double v, long ida, long idb, const std::string op )	: _value(v), _key(_cnt++), _op(op), _pred1(ida), _pred2(idb) {
      DBG1(" dco_flattener :: binary constructor");
      ++_cntops;
      vec_nodes.push_back(node(_section,_key,_op,_value,_pred1,_pred2));
    }

    // unary constructor
    type( const double v, const long ida, const std::string op ) : _value(v), _key(_cnt++), _op(op), _pred1(ida), _pred2(-1) {
      DBG1(" dco_flattener :: unary constructor");
      _lasgn = false;
      ++_cntops;
      vec_nodes.push_back(node(_section,_key,_op,_value,_pred1,-1));
    }

    // copy constructor
    type( const type & a ): _value(a._value), _key(_cnt++), _op("="), _pred1(a._key) {
      DBG1(" dco_flattener :: copy constructor");
      _lasgn = false;
      ++_cntops;
      vec_nodes.push_back(node(_section,_key,_op,_value,_pred1,-1));
    }

    // silent constructor : no src no dot output,
    // used to recreate existing node in postfix ++ and --
    type( double v, long id ) : _value(v), _key(id) {
      DBG1(" dco_flattener :: silent constructor");
      //_lasgn = false;
    }

    ///////////////
    // access routines
    ///////////////
    /** \brief Return key */
    long key() const { return _key; }
    /** \brief Return operation as string */
    std::string op() const { return _op; }
    /** \brief Return double value */
    double value() const { return _value; }
    /** \brief Return total number of nodes */
    static long cnt() { return _cnt; }
    /** \brief Return total number of operations */
    static long cntops() { return _cntops; }

    ///////////////
    // access routines that return references / change internal data
    ///////////////

    double& value() { return _value; }

    ///////////////
    // assignment
    ///////////////

    type& operator=( const type &a ) {
      if ( this == &a ) return *this;
      _value = a._value;
      if ( _lasgn ) {
        _key = _cnt++;
        DBG3(" dco_flattener :: assignment :: ",_key,a._key);
        vec_nodes.push_back(node(_section,_key,"=",_value,a._key,-1));
      }	else {
        _key = a._key;
        DBG2(" dco_flattener :: asignment :: re-use asgn to v",a._key);
      }
      _lasgn = true;
    }

    type& operator=( const double &a ) {
      DBG1(" dco_flattener :: passive assignment ");
      _value = a;
      _key = _cnt++;
      DBG2(" dco_flattener :: assignment :: ",_key);
      _op = "C";
      //++_cntinp;
      vec_nodes.push_back(node(_section,_key,_op,_value,-1,-1));
      _lasgn = true;
    }

    type& operator=( const int &a ) {
      DBG1(" dco_flattener :: passive assignment ");
      _value = a;
      _key = _cnt++;
      DBG2(" dco_flattener :: assignment :: ",_key);
      _op = "C";
      //++_cntinp;
      vec_nodes.push_back(node(_section,_key,_op,_value,-1,-1));
      _lasgn = true;
    }

    ///////////////
    // friends  : unary operators
    ///////////////

    #define UnaOp(AT,OP) friend const AT operator OP( const AT &a ) { \
      _lasgn = false; \
      return type( OP a.value(), a.key(), #OP ); \
    };
    UnaOp(type,+)
    UnaOp(type,-)

    ///////////////
    // friends  : binary operator
    ///////////////

    #define BinOp(AT,OP) friend const AT operator OP( const AT &a, const AT &b ) { \
      _lasgn = false; \
      return type( a.value() OP b.value(), a.key(), b.key(), #OP); \
    };
    BinOp(type,+)
    BinOp(type,-)
    BinOp(type,*)
    BinOp(type,/)

    ///////////////
    // friends  : compound assignment operators
    ///////////////

    #define BinOpRef(AT,OP) friend const AT& operator OP( AT &a, const AT &b ) { \
      _lasgn = false; \
      a._value OP b.value(); \
      a = type( a.value(), a.key(), b.key(), #OP); \
      return a; \
    };
    BinOpRef(type,+=)
    BinOpRef(type,-=)
    BinOpRef(type,*=)
    BinOpRef(type,/=)

    ///////////////
    // friends  : increment/decrement operators
    ///////////////

    #define IncDecOp(AT,OP) \
    AT& operator OP( ) { \
      OP(_value);	\
      long ia_old = _key; \
      _key = _cnt++; \
      _lasgn = false;	\
      DBG1(" dco_flattener :: increment/decrement operator");	\
      vec_nodes.push_back(node(_section,_key,#OP,_value,ia_old,-1)); \
      return *this;	\
    }; \
    AT operator OP( int ignore ) { \
      long ia_old = _key; \
      double va_old = _value; \
      OP(_value);	\
      _key = _cnt++; \
      _lasgn = false; \
      DBG1(" dco_flattener :: increment/decrement operator");	\
      vec_nodes.push_back(node(_section,_key,#OP,_value,ia_old,-1)); \
      return type( va_old, ia_old ); \
    };
    IncDecOp(type,++)
    IncDecOp(type,--)

    ///////////////
    // friends  : unary intrinsics
    ///////////////

    #define UnaIn(AT,OP) friend AT OP( const AT &a ) { \
      _lasgn = false; \
      return type( OP(a.value()), a.key(), #OP ); \
    };
    UnaIn(type,fabs)
    UnaIn(type,abs)
    UnaIn(type,round)
    UnaIn(type,ceil)
    UnaIn(type,floor)
    UnaIn(type,sqrt)
    UnaIn(type,cos)
    UnaIn(type,sin)
    UnaIn(type,tan)
    UnaIn(type,acos)
    UnaIn(type,asin)
    UnaIn(type,atan)
    UnaIn(type,cosh)
    UnaIn(type,sinh)
    UnaIn(type,tanh)
    UnaIn(type,exp)
    UnaIn(type,log)
    UnaIn(type,log10)

    ///////////////
    // friends  : binary intrinsics
    ///////////////

    #define BinIn(AT,OP) friend AT OP( const AT &a, const AT &b ) {	\
      _lasgn = false; \
      return type( OP(a.value(),b.value()), a.key(), b.key(), #OP ); \
    };
    BinIn(type,atan2)
    BinIn(type,pow)

    ///////////////
    // friends  : comparison operators
    ///////////////

    #define CmpOp(AT,OP) friend bool operator OP( const AT &a, const AT &b ) { \
      return a.value() OP b.value(); \
    };
    CmpOp(type,==)
    CmpOp(type,!=)
    CmpOp(type,< )
    CmpOp(type,<=)
    CmpOp(type,> )
    CmpOp(type,>=)



    ///////////////
    // friends  : min and max function
    ///////////////

    friend type min(const type &a, const type &b ) {
      _lasgn = false; \
      return type( std::min(a.value(),b.value()), a.key(), b.key(), "min" );
    }

    friend type max(const type &a, const type &b ) {
      _lasgn = false; \
      return type( std::max(a.value(),b.value()), a.key(), b.key(), "max" );
    }

    ///////////////
    // other stuff
    ///////////////

    static void set_sec( std::string name ) {
      _section = name;
    }

    std::string str() const {
      std::stringstream s, sstr1, sstr2, sstr3;
      s << _value;
      return s.str();
    }

    friend std::ostream& operator<<( std::ostream &s, const type &a) {
      s << a.str();
      return s;
    }


  }; //TYPE

  long dco::ia1s::type::_cnt    = 0;
  long dco::ia1s::type::_cntops = 0;
  bool dco::ia1s::type::_lasgn  = false;
  std::string dco::ia1s::type::_section  = "";

  ///////////////
  // flattener :: register inputs and outputs
  ///////////////

  static void register_input( type &a, double lb, double ub, std::string name="") {
    if(!vec_nodes[ a.key() ].is_inp()) _ninp++;
    vec_nodes[ a.key() ].set_inp();
    vec_nodes[ a.key() ].set_iv(interval(lb,ub));
    vec_nodes[ a.key() ].set_info(name);
    //PPINP( a.id() << " " << a.v()-h/2 << " " << a.v()+h/2 << " " << i3 << " " << i4 );
  }

  static void register_split( type &a, double beta, double gamma ) {
    split_nodes.push_back( std::make_tuple( a.key(), beta, gamma) );
  }

  static void register_variable( type &a, std::string name="") {
    if(!vec_nodes[ a.key() ].is_int())_nint++;
    vec_nodes[ a.key() ].set_int();
    vec_nodes[ a.key() ].set_info(name);
  }

  static void register_output( type &a, double iadj, std::string name="") {
    if(!vec_nodes[ a.key() ].is_out()) _nout++;
    vec_nodes[ a.key() ].set_out();
    vec_nodes[ a.key() ].set_a1s(interval(iadj,iadj));
    vec_nodes[ a.key() ].set_info(name);
  }

  static void set_section( std::string name="") {
    dco::ia1s::type::set_sec(name);
    auto iter = colors.find(name);
    if (iter == colors.end()) colors[name] = colors.size();
  }

  }
}
#endif // __DCOS_TYPE__
