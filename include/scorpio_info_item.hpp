#ifndef  __SCORPIO_INFO_ITEM__
#define  __SCORPIO_INFO_ITEM__

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "interval/interval.hpp"
//typedef filib::interval<filib_intertval_basetype> interval;


#include "dco_scorpio.hpp"

// serialization support with cereal 
//   http://uscilab.github.io/cereal/
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>


class scorpio_info_item 
{
public:
  //------------------
  // default constructor for use with containers 
  //------------------
  scorpio_info_item( )  : _value(0), _adjoint(interval::EMPTY()), 
			  _name(""), _file(""), _line(0), _comment(""),
			  _tsk1(-1), _tsk2(-1), _tsk3(-1), 
			  _tsk4(-1), _tsk5(-1), _tsk6(-1),
        _pvc(-3) { }

  // super constructor, should handle all data members 
  scorpio_info_item( dco::ia1s::type& x,  std::string n,
		     std::string f, int l,
		     int t1=-1, int t2=-1, int t3=-1, 
		     int t4=-1, int t5=-1, int t6=-1, std::string c = ""
		     )  : _value(interval(0)), _adjoint(interval::EMPTY()), 
			  _name(n), _file(f), _line(l), _comment(c),
			  _tsk1(t1), _tsk2(t2), _tsk3(t3), _tsk4(t4),
			  _tsk5(t5), _tsk6(t6),
			  _pvc(-3) {
    //    std::cout << " scorpio_info_item " << n << "  " << i1 << std::endl;
    _value = dco::ia1s::vec_nodes[x.key()].iv();
    _pvc   = x.key();
    // std::cout << "_pvc = " << _pvc << "\n";
  }

  // other constructors, using constructor delegation (C++11)
  scorpio_info_item( std::string f, int l,
         dco::ia1s::type& x,  std::string n,
		     int t1=-1, int t2=-1, int t3=-1, 
		     int t4=-1, int t5=-1, int t6=-1, std::string c = ""
         )  :   scorpio_info_item( x,n,f,l,t1,t2,t3,t4,t5,t6) { }
  scorpio_info_item( dco::ia1s::type& x,  std::string n,
		     int t1=-1, int t2=-1, int t3=-1, 
		     int t4=-1, int t5=-1, int t6=-1, std::string c = ""
         )  :   scorpio_info_item( x,n,"",-1,t1,t2,t3,t4,t5,t6) { }

  
  //------------------
  // internal communication with dco_scorpio
  //------------------

  bool extract_adjoint( interval &adj ,interval &val )  const {
    bool strd = false;
    if ( _pvc >= 0 ) {
      if ( _pvc <= dco::ia1s::vec_nodes.size() ) {
	// std::cerr << " scorpio_info_item: extract_adjoint() I"
	// 	  << " :: pvc " << _pvc << " mxtc " 
  // 	  <<  dco::ia1s::vec_nodes.size() << std::endl;
  adj = dco::ia1s::vec_nodes[_pvc].a1s();
  val = dco::ia1s::vec_nodes[_pvc].iv();
	strd = true;
      }
      else {
	std::cerr << " ERROR:: scorpio_info_item: extract_adjoint()"
		  << " R :: pvc (" << _pvc << ") to big,  mxtc : " 
      <<  dco::ia1s::vec_nodes.size() << std::endl;
      }
    }
    return strd;
  }

  void store_adjoint()    {
    _stored = extract_adjoint(_adjoint,_value);
  }

  //  compute default significance criteria to be stored for file writing
  void compute_significance( int i) {
    _significance =  significance( i );
  }
  
  //  significance retrivial methods
  double      significance( int i = 0 ) const {
    double    sig = 0;
    interval  adj = adjoint();
    interval  val = value();
    // if ( interval::EMPTY() == adj )  
    //   return -1.0;
    switch ( i )  { 
    case 0 :
      return abs(adj).sup()*width(val);
    case 1 :
      return width(val*adj);
    case 2 :
      return width(adj);
//    case 3 :
//      return width(final);
//    case 4 :
//      return abs(adj) * width(final) / width(val);
//    case 5 :
//      return abs(adj) * width(val) / min(abs(final));
    default : 
      return abs(adj).sup()*width(val);
    }
    return sig;
  }

  //------------------
  // other data retrivial methods
  //------------------

  interval    value()     const { return _value;  }

  interval    adjoint()   const { 
    // if adjoint already stored, take it
    if ( _stored )    return _adjoint;
    // if not: extract adjoint
    //   1st : take default value
    interval adj = _adjoint;
    interval val = _value;
    //   2nd : extract adjoint, will also store theadjoint in _adjoint, 
    //            if adjoint is available.
    //         If no adjoint is available, default adjoint remains in adj. 
    extract_adjoint( adj, val );
    return adj;
  }

  std::string name()      const { return _name; }
  std::string file()      const { return _file; }
  int         line()      const { return _line; }
  int         tsk1()      const { return _tsk1; }
  int         tsk2()      const { return _tsk2; }
  int         tsk3()      const { return _tsk3; }
  int         tsk4()      const { return _tsk4; }
  int         tsk5()      const { return _tsk5; }
  int         tsk6()      const { return _tsk6; }
  std::string comment()   const { return _comment; }

  //------------------
  //  transformation to std::string for stream output
  //------------------
  std::string str()   const { 
    std::stringstream s, sstr1, sstr2, sstr3; 
    s << "({ " 
      << "sig: " << std::setw(15) << _significance << ",";
    sstr2 << value();
    s << " v: "  << std::setw(22) << sstr2.str() << "," ;
    sstr3 << adjoint();
    s  << " a: "  << std::setw(22) << sstr3.str()  << ", " 
       << std::setw(4) << tsk1() << std::setw(4) << tsk2() 
       << std::setw(4) << tsk3() << std::setw(4) << tsk4() 
       << std::setw(4) << tsk5() << std::setw(4) << tsk6() << ", " 
       << name();
     if ( 0 < _file.length() ) 
        s << "," << file();
    if ( 0 < line() ) 
      s << ":" << line() ;
    if ( 0 < _comment.length() ) 
      s << ", " << comment() ;
    s << " })";
    return s.str();
  };

  // Serialization support
  template<class Archive>
  void serialize(Archive & archive)
  {
    // serialize things by passing them to the archive
    _significance = significance();
    archive( _significance, _value, _adjoint, _name, _file, _line, _tsk1, _tsk2, _tsk3, _tsk4, _tsk5, _tsk6 );
    // archive( _significance, _name, _line, _tsk1, _tsk2, _tsk3, _tsk4, _tsk5, _tsk6 );
  }


private :
  interval          _value, _adjoint;
  long              _pvc  = -3;    // node index
  std::string       _name, _file;
  int               _line;
  int               _tsk1=-1, _tsk2=-1, _tsk3=-1, _tsk4=-1, _tsk5=-1, _tsk6=-1;
  std::string       _comment;
  //   adjoint stored internally or not ??
  bool              _stored = false;
  // significance read from file
  double            _significance = -1.0;

}; // class scorpio_info_item



//------------------
//  stream output operator 
//------------------

std::ostream& operator<<( std::ostream &s, const scorpio_info_item &si) {
  s << si.str();
  return s;
}


//------------------
//  serialization support for filib intervals
//------------------

// serialization support with cereal 
//   http://uscilab.github.io/cereal/
#include <cereal/archives/binary.hpp>

namespace cereal
{
  //! Serializing (save) for interval from filib
  template <class Archive> inline
  void save( Archive & ar, interval const & iv )
  {
    ar( _CEREAL_NVP("lower bound", iv.inf() ),
	_CEREAL_NVP("upper bound", iv.sup() ) );
  }

  //! Serializing (load) for interval from filib
  template <class Archive> inline
  void load( Archive & ar, interval& bits )
  {
    filib_interval_basetype inf, sup;
    ar( _CEREAL_NVP("lower bound", inf ),
	_CEREAL_NVP("upper bound", sup ) );
    bits = interval( inf, sup );
  }


  // //! Serializing for interval from filib  
  // template <class Archive> inline
  // void serialize( Archive & ar, interval& iv )
  // {
  //   ar( _CEREAL_NVP("lower bound", iv.inf() ),
  //       _CEREAL_NVP("upper bound", iv.sup() ) );
  // }

} // namespace cereal


#endif // __SCORPIO_INFO_ITEM__
