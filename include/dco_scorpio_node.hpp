#ifndef  __DCOS_NODE__
#define  __DCOS_NODE__

#include "dco_scorpio.hpp"

namespace dco {
  namespace ia1s {
    class node {
    private:
      long _key;
      bool _inp = false, _int = false, _out = false, _sig = true;
      double _sigval;
      interval _iv, _a1s;
      std::string _op;
      std::string _section, _name;

    public:
      std::vector<long> _succ;
      std::vector<long> _pred;

      node() { }

      node( std::string section, long key, std::string op, double value, long ida=-1, long idb=-1 ) : _key(key), _op(op), _iv(interval(value)), _a1s(interval(0)), _section(section) {
        if (ida != -1) {
          _pred.push_back(ida);
          vec_nodes[ida]._succ.push_back(_key);
          if (idb != -1) {
            _pred.push_back(idb);
            vec_nodes[idb]._succ.push_back(_key);
          }
        }
      }

      node( std::string section, long key, std::string op, interval ivalue, long ida=-1, long idb=-1 ) : _key(key), _op(op), _iv(ivalue), _a1s(interval(0)), _section(section) {
        if (ida != -1) {
          _pred.push_back(ida);
          vec_nodes[ida]._succ.push_back(_key);
          if (idb != -1) {
            _pred.push_back(idb);
            vec_nodes[idb]._succ.push_back(_key);
          }
        }
      }

      /** \brief Return key */
      long key() const { return _key; }
      /** \brief Return operation as string */
      std::string op() const { return _op; }
      /** \brief Return label of section as string */
      std::string section() const { return _section; }
      /** \brief Return significance value */
      double sigval() const { return _sigval; }
      /** \brief Return value interval */
      interval iv() const { return _iv; }
      /** \brief Return adjoint interval */
      interval a1s() const { return _a1s; }
      /** \brief Return true if node is significant */
      bool sig() const { return _sig; }
      /** \brief Return true if node is input */
      bool is_inp() const { return _inp; }
      /** \brief Return true if node is intermediate */
      bool is_int() const { return _int; }
      /** \brief Return true if node is output */
      bool is_out() const { return _out; }
      std::string name() const { return _name; }

      /** \brief Set node to constant value */
      void set_const(double c) { _iv = c; }
      /** \brief Set significance value */
      void set_sigval(double v) { _sigval = v; }
      /** \brief Set section label */
      void set_sec(std::string name) { _section = name; }
      /** \brief Set value interval of node */
      void set_iv(interval value) { _iv = value; }
      /** \brief Set adjoint interval of node */
      void set_a1s(interval a1s) { _a1s = a1s; }
      /** \brief Mark node as insignificant */
      void set_insig() { _sig = false; }
      /** \brief Mark node as significant */
      void set_sig() { _sig = true; }
      /** \brief Mark node as input */
      void set_inp() { _inp = true; }
      /** \brief Mark node as intermediate */
      void set_int() { _int = true; }
      /** \brief Mark node as output */
      void set_out() { _out = true; }
      /** \brief Set info */
      void set_info( std::string n ) {
        _name = n;
      }

      void reset() {
        if(_pred.size() != 0) {
          //_iv = interval::EMPTY();
        }
        _a1s = interval::EMPTY();
        _sigval = 0;
        _sig = true;
        _out = false;
      }
    }; // NODE
  }
}

#endif // __DCOS_NODE__
