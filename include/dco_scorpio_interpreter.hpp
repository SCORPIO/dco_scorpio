#ifndef  __DCOS_INTERPRETER__
#define  __DCOS_INTERPRETER__

namespace dco {
  namespace ia1s {
    enum StringOps{ evEq,
                    evPlus,
                    evMinus,
                    evTimes,
                    evDivide,
                    evSqrt,
                    evSin,
                    evCos,
                    evTan,
                    evAsin,
                    evAcos,
                    evAtan,
                    evSinh,
                    evCosh,
                    evTanh,
                    evExp,
                    evLog,
                    evLog10,
                    evRound,
                    evCeil,
                    evFloor,
                    evAbs,
                    evMin,
                    evMax,
                    evPow,
                    evC,
                    evEnd };

    /** \brief Returns the number of total computations */
    std::vector<long> count_op() {
      std::vector<long> n(colors.size()+1,0);
      for ( auto & it : vec_nodes ) {
        if ( it.op() != "C" || it.is_inp() ) {
          n[0]++;
          n[colors[it.section()]]++ ;
        }
      }
      return n;
    }

    /** \brief Returns the number of insignificant computations */
    long count_insig() {
      long n = 0;
      for ( auto & it : vec_nodes ) {
        if (!it.sig() && (it.op() != "C" || it.is_inp()) ) {
          n++;
        }
      }
      //std::cout<<"Number of insignificant computations: " << n << "\n";
      return n;
    }

    /** \brief Read input intervals with index of corresponding node */
    void read_scenario(long sid, long &last_split) {
      //split.clear();
      std::ifstream file; // value input file
      file.open("data/analyse/scenario"+std::to_string(sid)+".txt");
      if ( file.is_open() && file.peek() != std::ifstream::traits_type::eof() ) {
        file >> _nsplit;
        //std::cout << "_nsplit " << _nsplit << "\n";
        if( _nsplit > 0 ) {
          while ( file.good() ) {
            double lb, ub;
            file >> last_split >> lb >> ub;
            split[last_split] = interval(lb,ub);
          }
        }
        file.close();
//        std::cout << "Split list after/in read_scenario:\n";
//        for ( auto it=split.begin(); it!=split.end(); ++it ) {
//          std::cout << it->first << " " << it->second.inf() << " " << it->second.sup() << "\n";
//        }
      } else {
        std::cout << "Unable to open file scenario" + std::to_string(sid) + ".txt in read_scenario\n";
      }

//      for ( auto it=split.begin(); it!=split.end(); ++it ) {
//        std::cout << it->first << " " << it->second.inf() << " " << it->second.sup() << "\n";
//      }
//      std::cout << "\n";
    };

    /** \brief Random interval splitting */
    void write_scenario(long sid, std::string fn = "analyse") {
      std::ofstream file;
      file.open("data/"+fn+"/scenario"+std::to_string(sid)+".txt");
      file << _nsplit << std::endl;
      if (file.is_open()) {
        //std::cout << "Split list in write_scenario:\n";
        for ( auto it=split.begin(); it!=split.end(); ++it ) {
          //std::cout << it->first << " " << it->second << "\n";
          file << it->first << " " << it->second.inf() << " " << it->second.sup() << "\n";
        }
        file.close();
      } else {
        std::cout << "Unable to open file scenario" + std::to_string(sid) + ".txt in write_scenario\n";
      }
    };

    /** \brief Random interval splitting */
    void write_ranking(long sid, long insig) {
      std::ofstream file;
      file.open("data/rank/rank"+std::to_string(sid)+".txt");
      if (file.is_open()) {
        std::vector<long> op = count_op();
        file << _nsplit << " " << insig << " " << op[0] << "\n";
        file.close();
      } else {
        std::cout << "Unable to open file rank" + std::to_string(sid) + ".txt in write_ranking\n";
      }
    };

    void splitting(long& sid, long start, long& vid) {
      for ( auto & it : split_nodes ) {
        long key = std::get<0>(it);
        if ( key >= start) {
          if ( vec_nodes[ key ].sig() ) {
            if ( vec_nodes[ key ].iv().diam() > std::get<1>(it) /*&& vec_nodes[ key ].a1s().diam() < std::get<2>(it)*/ ) { // Split condition
              _nsplit++;
              vid = key;
              split[vid] = interval(vec_nodes[ key ].iv().mid(),vec_nodes[ key ].iv().sup());
              write_scenario(2*sid+1);
              split[vid] = interval(vec_nodes[ key ].iv().inf(),vec_nodes[ key ].iv().mid());
              sid *= 2;
              break;
            }
          }
        }
      }
    }

    /** \brief Creates a dot-file with default name graph.dot that contains the computational graph */
    void write_dot(long sid = 0, std::string fn = "data/graph/graph") {
      std::ofstream file; // DOT output file
      if (sid != 0 ) file.open(fn+std::to_string(sid)+".dot");
      else file.open(fn+".dot");
      if (file.is_open()) {
        file.precision(2);
        file.setf(std::ios::fixed);
        file.setf(std::ios::showpoint);
        file << "digraph {\n";
        file << "graph[ratio=2.5, margin=0, center=true, nodesep=0.2, ranksep=0.2, rankdir=TB]\n";

        file << "node[fontsize=30, fontcolor=black, shape=circle, style=filled, colorscheme=\"set28\"]\n";

        //file << "IN  [label=\"INPUT\", fontsize=6, color=blue, fontcolor=black, style=filled, fillcolor=\"#DCDCFF\"];\n";
        //file << "OUT  [label=\"OUTPUT\", fontsize=6, color=blue, fontcolor=black, style=filled, fillcolor=\"#DCDCFF\"];\n";

        bool lastFS = true;
        long last = 0;
        long llast = 0;
        for ( auto & it : vec_nodes ) {
          if( !( it._succ.empty() && it._pred.empty() ) || it.is_inp() || it.is_out() ) {
            file << it.key() << " [";

            file << "label=\"";
            file << it.name();
            if (it.is_inp())
              file << "\\nIN";
            else
              file << "\\n" << it.op();
            file << "\\n" << it.sigval();
            file << "\", ";

            //file << "group = \"" << it.section() << "\",";
            file << " fillcolor=" << colors[it.section()]+1 << ", ";
            if (it.is_out() || it.is_inp()) {
              file << "penwidth=7"; // , fillcolor=\"#DCDCFF\"
            } else if (it.sig()) {
              file << ""; // , fillcolor=\"#DCDCFF\"
            } else {
              file << "color=\"#FF0000\", penwidth=7"; // shape=rect, fillcolor=\"#FF6464\"
            }
            file << "];";
            //if (it.is_out())
            //  file << it.key() << " -> OUT; ";
            for ( auto p_it : it._pred )
              file << p_it << " -> " << it.key() << ";";

            if ( lastFS ) {
              if ( it.section() == "BS" ) {
                file << last << " -> " << it.key() << "[style=invis];";
                lastFS = false;
              }
            } else {
              if ( it.section() != "BS" ) {
                file << llast << " -> " << last << "[style=invis];";
                lastFS = true;
              }
            }

            llast = last;
            last = it.key();

            file << "\n";
          }
        }
        file << "}\n";
      } else {
        std::cout << "Unable to open file in write_dot\n";
      }
      file.close();
    }

    /** \brief Write the computational graph in a .cpp file with default name f0.cpp */
    void write_code(long sid, long start, long end, std::string fn = "data/code/f") {
      std::map<std::string,int> opmap;
      opmap["="]     = evEq;
      opmap["+"]     = evPlus;
      opmap["+="]    = evPlus;
      opmap["-"]     = evMinus;
      opmap["-="]    = evMinus;
      opmap["*"]     = evTimes;
      opmap["*="]    = evTimes;
      opmap["/"]     = evDivide;
      opmap["/="]    = evDivide;
      opmap["sqrt"]  = evSqrt;
      opmap["sin"]   = evSin;
      opmap["cos"]   = evCos;
      opmap["tan"]   = evTan;
      opmap["asin"]  = evAsin;
      opmap["acos"]  = evAcos;
      opmap["atan"]  = evAtan;
      opmap["sinh"]  = evSinh;
      opmap["cosh"]  = evCosh;
      opmap["tanh"]  = evTanh;
      opmap["exp"]   = evExp;
      opmap["log"]   = evLog;
      opmap["log10"] = evLog10;
      opmap["pow"]   = evPow;
      opmap["round"] = evRound;
      opmap["ceil"]  = evCeil;
      opmap["floor"] = evFloor;
      opmap["abs"]   = evAbs;
      opmap["fabs"]  = evAbs;
      opmap["min"]   = evMin;
      opmap["max"]   = evMax;
      opmap["C"]     = evC;

      std::cout << "Write f" << sid << ".cpp for " << start << " to " << end << std::endl;
      if ( start == -1 ) start = 0;
      if ( start <= end ) {
        std::ofstream file; // Code output file
        file.open(fn+std::to_string(sid)+".cpp");
        if (file.is_open()) {
          file << "#include \"f.h\"\n";
          file << "void f" << sid << "() {\n";
          for ( long i = start; i < end; i++ ) {
            auto it = vec_nodes[i];
            if ( (!it.is_inp() && !it._succ.empty()) || it.is_out() ) {
              file << "  v" << it.key() << " = ";
              if( !it.sig() ) {
                file << it.iv().mid() << ";\n";
              } else {
                switch( opmap[it.op().c_str()] ) {
                  case evEq :
                    {
                    file << "v" << it._pred[0] << ";\n" ;
                    break;
                    }

                  case evPlus :
                    {
                    file << "v" << it._pred[0] << " + v" << it._pred[1] << ";\n" ;
                    break;
                    }

                  case evMinus :
                    {
                    if(it._pred.size() == 1) {
                      file << " -v" << it._pred[0] << ";\n";
                    } else {
                      file << "v" << it._pred[0] << " - v" << it._pred[1] << ";\n";
                    }
                    break;
                    }

                  case evTimes :
                    {
                    file << "v" << it._pred[0] << " * v" << it._pred[1] << ";\n" ;
                    break;
                    }

                  case evDivide :
                    {
                    if(it._pred.size() == 1) {
                      file << " 1.0/v" << it._pred[0] << ";\n";
                    } else {
                      file << "v" << it._pred[0] << " / v" << it._pred[1] << ";\n";
                    }
                    break;
                    }

                  case evPow:
                    {
                    file << "pow( v" << it._pred[0] << ", v" << it._pred[1] << " );\n";
                    break;
                    }

                  case evSqrt :
                    {
                    file << "sqrt( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evSin :
                    {
                    file << "sin( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evCos :
                    {
                    file << "cos( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evTan :
                    {
                    file << "tan( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evAsin :
                    {
                    file << "asin( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evAcos :
                    {
                    file << "acos( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evAtan :
                    {
                    file << "atan( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evSinh :
                    {
                    file << "sinh( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evCosh :
                    {
                    file << "cosh( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evTanh :
                    {
                    file << "tanh( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evExp :
                    {
                    file << "exp( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evLog :
                    {
                    file << "log( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evLog10 :
                    {
                    file << "log10( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evRound :
                    {
                    file << "round( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evCeil :
                    {
                    file << "ceil( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evFloor :
                    {
                    file << "floor( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evAbs :
                    {
                    file << "abs( v" << it._pred[0] << " );\n";
                    break;
                    }

                  case evMin :
                    {
                    file << "min( v" << it._pred[0] << it._pred[1] << " );\n";
                    break;
                    }

                  case evMax :
                    {
                    file << "max( v" << it._pred[0] << it._pred[1] << " );\n";
                    break;
                    }

                  case evC :
                    {
                    file <<  it.iv().sup() << ";\n";
                    break;
                    }
                  default:
                    std::cout << it.op().c_str() << "Operation not available in write code\n" << std::endl;
                    break;
                }
              }
            }
            //if ( it.is_out() ) file << "  y[" << iout++ << "] = v" << it.key() << " ;\n";
          }
          if ( end < vec_nodes.size() ) {
            file << "  if(v" << end << " < " << vec_nodes[end].iv().mid() << ") {\n";
            file << "    f" << 2*sid << "();\n";
            file << "  } else {\n";
            file << "    f" << 2*sid+1 << "();\n";
            file << "  }\n";
          }
          file << "}\n";
        } else {
          std::cout << "Unable to open file f" << sid << ".cpp in write_code\n";
        }
        file.close();
      }
    };

    void write_code(std::string fn = "data/code/f") {
      write_code(1,0,vec_nodes.size(),fn);
    }

    void write_code_init(std::string fn = "data/code/f") {
      std::ofstream file;                   // Code output file
      file.open(fn+".cpp");
      if (file.is_open()) {
        file << "#include <vector>\n";
        file << "#include <cmath>\n";
        file << "#include <iostream>\n";
        file << "void f0();\n";
        file << "void f1();\n";
        file << "double";
        for ( long i=0; i < vec_nodes.size()-1; i++)
          file << " v" << i << ", ";
        file << " v" << vec_nodes.size()-1 << ";\n";
        file << "void f(std::vector<double>& x, std::vector<double>& y) {\n";
        long iin = 0, iout = 0;
        for ( auto & it : vec_nodes ) {
          if (it.is_inp()) {
            file << "  v" << it.key() << " = x[" << iin++ << "];\n";
          }
        }
        file << "  if ( ";
        for ( auto & it : vec_nodes ) {
          if (it.is_inp()) {
            file << "v" << it.key() << " >= " << it.iv().inf() << " && ";
            file << "v" << it.key() << " <= " << it.iv().sup();
            if (--iin != 0) file << " && ";
          }
        }
        file << ") {\n";
        file << "    f1();\n";
        file << "  } else {\n";
        file << "    f0();\n";
        file << "  }\n";
        for ( auto & it : vec_nodes ) {
          if ( it.is_out() ) file << "  y[" << iout++ << "] = v" << it.key() << " ;\n";
        }
        file << "}\n\n";
        file << "int main() {\n";
        file << "  std::vector<double> x(" << _ninp << "), y(" << _nout << ");\n";
        file << "  //TODO: x = \n";
        file << "  f(x,y);\n";
        file << "  std::cout << \"y =\";\n";
        file << "  for ( auto & it : y ) {\n";
        file << "    std::cout << ' ' << it;\n";
        file << "    std::cout << \"\\n\";\n";
        file << "  }\n";
        file << "}\n";
      } else {
        std::cout << "Unable to open file f.cpp in write_code\n";
      }
      file.close();
      file.open(fn+".h");
      if (file.is_open()) {
        file << "#include <cmath>\n";
        file << "extern double";
        for ( long i=0; i < vec_nodes.size()-1; i++)
          file << " v" << i << ", ";
        file << " v" << vec_nodes.size()-1 << ";\n";
      } else {
        std::cout << "Unable to open file f.h in write_code\n";
      }
      file.close();
      write_code(0,0,vec_nodes.size());
    };

    /** \brief Print the graph in the shell in the format
     * #in: [total number of inputs]
     * #out: [total nummber of outputs]
     * #ops: [total number of unary and binary operations]
     * #nodes: [total number of nodes ( #ops + #constants )]
     * [key] [operation] Val: [value interval] A1S: [adjoint interval] Sig: [significance value] Succ: [#Succ] In: [input?] Out: [output?]
     * {for all nodes that have a successor or outputs}
     */
    void print_graph(long sid = 0) {
      std::vector<long> op = count_op();
      std::cout << "\nScenario " << sid << ":\n";
      std::cout << "#ops: " << op[0] << "\n";
      std::cout << "#nodes: " << vec_nodes.size() << "\n";
      for ( auto it : vec_nodes ) {
        //if(!it._succ.empty() || it.is_inp() || it.is_out()) {
          std::cout << std::setprecision(5);
          std::cout << it.key() << "\t  ";
          std::cout << it.op() << "\t ";
          std::cout << "Val: " << it.iv() << "\t ";
          std::cout << "A1S: " << it.a1s() << "\t ";
          std::cout << "Sig: " << it.sigval() << "\t";
          std::cout << "Succ: " << it._succ.size() << "\t";
          //std::cout << "P: ";
          //for ( int i=0; i < it._pred.size(); i++ )
          //  std::cout << it._pred[i] << "(" << it._pd[i] << ") ";
          std::cout << "\tS: ";
          for ( auto & s_it : it._succ )
            std::cout << s_it << " ";
          //std::cout << "\tIn: " << it.is_inp();
          //std::cout << "\tOut: " << it.is_out();
          std::cout << "\n";
        //}
      }
      std::cout << "\n";
    }

    /** \brief Print the graph in the shell in the Format
     * #in: [total number of inputs]
     * #out: [total nummber of outputs]
     * [key] [operation] Val: [value interval] A1S: [adjoint interval] Sig: [significance value]
     * {for all inputs and outputs}
     */
    void print_io() {
      std::cout << "#in: " << _ninp << "\n";
      std::cout << "#out: " << _nout << "\n";
      for ( auto & it : vec_nodes ) {
        if(it.is_inp() || it.is_out()) {
          std::cout << std::setprecision(5);
          std::cout << it.key() << "\t  ";
          std::cout << it.op() << "\t ";
          std::cout << "Val: " << it.iv() << "\t ";
          std::cout << "A1S: " << it.a1s() << "\t ";
          std::cout << "Sig: " << it.sigval() << "\t";
          std::cout << std::endl;
        }
      }
    }

    void print_iio() {
      std::cout << "#in: " << _ninp << "\n";
      std::cout << "#out: " << _nout << "\n";
      for ( auto & it : vec_nodes ) {
        if(it.is_inp() || it.is_int() || it.is_out()) {
          std::cout << std::setprecision(5);
          std::cout << it.key() << "\t  ";
          std::cout << it.op() << "\t ";
          std::cout << "Val: " << it.iv() << "\t ";
          std::cout << "A1S: " << it.a1s() << "\t ";
          std::cout << "Sig: " << it.sigval() << "\t";
          std::cout << std::endl;
        }
      }
    }

    /** \brief Value interpretation and calculation of partial derivatives */
    void interpret_value(long start = 0) {

      std::map<std::string,int> opmap;
      opmap["="]     = evEq;
      opmap["+"]     = evPlus;
      opmap["+="]    = evPlus;
      opmap["-"]     = evMinus;
      opmap["-="]    = evMinus;
      opmap["*"]     = evTimes;
      opmap["*="]    = evTimes;
      opmap["/"]     = evDivide;
      opmap["/="]    = evDivide;
      opmap["sqrt"]  = evSqrt;
      opmap["sin"]   = evSin;
      opmap["cos"]   = evCos;
      opmap["tan"]   = evTan;
      opmap["asin"]  = evAsin;
      opmap["acos"]  = evAcos;
      opmap["atan"]  = evAtan;
      opmap["sinh"]  = evSinh;
      opmap["cosh"]  = evCosh;
      opmap["tanh"]  = evTanh;
      opmap["exp"]   = evExp;
      opmap["log"]   = evLog;
      opmap["log10"] = evLog10;
      opmap["pow"]   = evPow;
      opmap["round"] = evRound;
      opmap["ceil"]  = evCeil;
      opmap["floor"] = evFloor;
      opmap["abs"]   = evAbs;
      opmap["fabs"]  = evAbs;
      opmap["min"]   = evMin;
      opmap["max"]   = evMax;
      opmap["C"]     = evC;

      for ( auto & it : vec_nodes ) {
        if ( it.key() >= start) {
          if ( split.find(it.key()) != split.end() ) {
              it.set_iv(split[it.key()]);
            } else {
              if ( !it.is_inp() ) {
              switch ( opmap[it.op().c_str()] ) {

                case evEq :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(A.iv());
                  break;
                  }

                case evPlus :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  auto & B = vec_nodes[ it._pred[1] ];
                  it.set_iv(A.iv()+B.iv());
                  break;
                  }

                case evMinus :
                  {
                    auto & A = vec_nodes[ it._pred[0] ];
                    if(it._pred.size() == 1) {
                      it.set_iv(-A.iv());
                    } else {
                      auto & B = vec_nodes[ it._pred[1] ];
                      it.set_iv(A.iv()-B.iv());
                    }
                  break;
                  }

                case evTimes :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  auto & B = vec_nodes[ it._pred[1] ];
                  it.set_iv(A.iv()*B.iv());
                  break;
                  }

                case evDivide :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  if(it._pred.size() == 1) {
                    it.set_iv(1.0/A.iv());
                  } else {
                    auto & B = vec_nodes[ it._pred[1] ];
                    it.set_iv(A.iv()/B.iv());
                  }
                  break;
                  }

                case evSqrt :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(sqrt(A.iv()));
                  break;
                  }

                case evSin :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(sin(A.iv()));
                  break;
                  }

                case evCos :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(cos(A.iv()));
                  break;
                  }

                case evTan :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(tan(A.iv()));
                  break;
                  }

                case evAsin :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(asin(A.iv()));
                  break;
                  }

                case evAcos :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(acos(A.iv()));
                  break;
                  }

                case evAtan :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(atan(A.iv()));
                  break;
                  }

                case evSinh :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(sinh(A.iv()));
                  break;
                  }

                case evCosh :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(cosh(A.iv()));
                  break;
                  }

                case evTanh :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(tanh(A.iv()));
                  break;
                  }

                case evExp :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(exp(A.iv()));
                  break;
                  }

                case evLog :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(log(A.iv()));
                  break;
                  }

                case evLog10 :
                  {
                  auto & A = vec_nodes[ it._pred[0] ];
                  it.set_iv(log10(A.iv()));
                  break;
                  }

               case evRound :
                 {
                 auto & A = vec_nodes[ it._pred[0] ];
                 it.set_iv(interval(round(inf(A.iv())), round(sup(A.iv()))));
                 break;
                 }

               case evCeil :
                 {
                 auto & A = vec_nodes[ it._pred[0] ];
                 it.set_iv(interval(ceil(inf(A.iv())), ceil(sup(A.iv()))));
                 break;
                 }

               case evFloor :
                 {
                 std::cout << "in floor\n";
                 auto & A = vec_nodes[ it._pred[0] ];
                 it.set_iv(interval(floor(inf(A.iv())), floor(sup(A.iv()))));
                 break;
                 }

               case evPow :
                 {
                   auto & A = vec_nodes[ it._pred[0] ];
                   auto & B = vec_nodes[ it._pred[1] ];
                   it.set_iv(pow(A.iv(),B.iv()));
                   break;
                  }

               case evAbs :
                 {
                 auto & A = vec_nodes[ it._pred[0] ];
                 it.set_iv(abs(A.iv()));
                 break;
                 }

               case evMin :
                 {
                 auto & A = vec_nodes[ it._pred[0] ];
                 auto & B = vec_nodes[ it._pred[1] ];
                 it.set_iv(A.iv().imin(B.iv()));
                 break;
                 }

               case evMax :
                 {
                 auto & A = vec_nodes[ it._pred[0] ];
                 auto & B = vec_nodes[ it._pred[1] ];
                 it.set_iv(A.iv().imax(B.iv()));
                 break;
                 }

                case evC :
                  {
                  break;
                  }
                default:
                  std::cout << "Operation " << it.op().c_str() << " not available\n" << std::endl;
                  break;
              } // switch
            } // if !input
          }
        }
      }
    }

    /** \brief Adjoint interpretation */
    void interpret_adjoint() {
          std::map<std::string,int> opmap;
          opmap["="]     = evEq;
          opmap["+"]     = evPlus;
          opmap["+="]    = evPlus;
          opmap["-"]     = evMinus;
          opmap["-="]    = evMinus;
          opmap["*"]     = evTimes;
          opmap["*="]    = evTimes;
          opmap["/"]     = evDivide;
          opmap["/="]    = evDivide;
          opmap["sqrt"]  = evSqrt;
          opmap["sin"]   = evSin;
          opmap["cos"]   = evCos;
          opmap["tan"]   = evTan;
          opmap["asin"]  = evAsin;
          opmap["acos"]  = evAcos;
          opmap["atan"]  = evAtan;
          opmap["sinh"]  = evSinh;
          opmap["cosh"]  = evCosh;
          opmap["tanh"]  = evTanh;
          opmap["exp"]   = evExp;
          opmap["log"]   = evLog;
          opmap["log10"] = evLog10;
          opmap["pow"]   = evPow;
          opmap["round"] = evRound;
          opmap["ceil"]  = evCeil;
          opmap["floor"] = evFloor;
          opmap["abs"]   = evAbs;
          opmap["fabs"]  = evAbs;
          opmap["min"]   = evMin;
          opmap["max"]   = evMax;
          opmap["C"]     = evC;

          for ( auto & it : vec_nodes ) {
            if(!it.is_out()) it.set_a1s(0);
          }

          for ( auto rit = vec_nodes.rbegin(); rit != vec_nodes.rend(); ++rit ) {

            if ( !rit->is_inp() ) {
              switch ( opmap[rit->op().c_str()] ) {

                case evEq :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s());
                  break;
                  }

                case evPlus :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  auto & B = vec_nodes[ rit->_pred[1] ];
                  A.set_a1s(A.a1s()+rit->a1s());
                  B.set_a1s(B.a1s()+rit->a1s());
                  break;
                  }

                case evMinus :
                  {
                    auto & A = vec_nodes[ rit->_pred[0] ];
                    if(rit->_pred.size() == 1) {
                      A.set_a1s(A.a1s()-rit->a1s());
                    } else {
                      auto & B = vec_nodes[ rit->_pred[1] ];
                      A.set_a1s(A.a1s()+rit->a1s());
                      B.set_a1s(B.a1s()-rit->a1s());
                    }
                  break;
                  }

                case evTimes :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  auto & B = vec_nodes[ rit->_pred[1] ];
                  A.set_a1s(A.a1s()+B.iv()*rit->a1s());
                  B.set_a1s(B.a1s()+A.iv()*rit->a1s());
                  break;
                  }

                case evDivide :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  if(rit->_pred.size() == 1) {
                    A.set_a1s(A.a1s()-1.0/(power(A.iv(),2.0))*rit->a1s());
                  } else {
                    auto & B = vec_nodes[ rit->_pred[1] ];
                    A.set_a1s(A.a1s()+1.0/(B.iv())*rit->a1s());
                    B.set_a1s(B.a1s()-A.iv()/power(B.iv(),2.0)*rit->a1s());
                  }
                  break;
                  }

                case evSqrt :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s()/(2.0*sqrt(A.iv())));
                  break;
                  }

                case evSin :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+cos(A.iv())*rit->a1s());
                  break;
                  }

                case evCos :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()-sin(A.iv())*rit->a1s());
                  break;
                  }

                case evTan :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s()/(cos(A.iv())*cos(A.iv()))); //abs(cos*cos)
                  break;
                  }

                case evAsin :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s()/sqrt(1.0-A.iv()*A.iv())); //abs(A*A)
                  break;
                  }

                case evAcos :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()-rit->a1s()/sqrt(1.0-A.iv()*A.iv())); //abs(A*A)
                  break;
                  }

                case evAtan :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s()/(1.0+A.iv()*A.iv())); //abs(A*A)
                  break;
                  }

                case evSinh :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+cosh(A.iv())*rit->a1s());
                  break;
                  }

                case evCosh :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+sinh(A.iv())*rit->a1s());
                  break;
                  }

                case evTanh :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s()/(cosh(A.iv())*cosh(A.iv()))); //abs(cosh*cosh)
                  break;
                  }

                case evExp :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+exp(A.iv())*rit->a1s());
                  break;
                  }

                case evLog :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s()/A.iv());
                  break;
                  }

                case evLog10 :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s()/(A.iv()*log(10)));
                  break;
                  }

                case evRound :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s());
                  break;
                  }

                case evCeil :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s());
                  break;
                  }

                case evFloor :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  A.set_a1s(A.a1s()+rit->a1s());
                  break;
                  }

                case evAbs :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  if (inf(A.iv())>0) {
                    A.set_a1s(A.a1s()+rit->a1s());
                  } else if (sup(A.iv())<0) {
                    A.set_a1s(A.a1s()-rit->a1s());
                  } else {
                    A.set_a1s(A.a1s()+interval(-1,1)*rit->a1s());;
                  }
                  break;
                  }

                case evMin :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  auto & B = vec_nodes[ rit->_pred[1] ];
                  A.set_a1s(A.a1s()+interval(0,1)*rit->a1s());
                  B.set_a1s(B.a1s()+interval(0,1)*rit->a1s());
                  break;
                  }

                case evMax :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  auto & B = vec_nodes[ rit->_pred[1] ];
                  A.set_a1s(A.a1s()+interval(0,1)*rit->a1s());
                  B.set_a1s(B.a1s()+interval(0,1)*rit->a1s());
                  break;
                  }

                case evPow :
                  {
                  auto & A = vec_nodes[ rit->_pred[0] ];
                  auto & B = vec_nodes[ rit->_pred[1] ];
                  A.set_a1s(A.a1s()+B.iv()*pow(A.iv(),B.iv()-1.0)*rit->a1s());
                  B.set_a1s(B.a1s()+pow(A.iv(),B.iv())*log(A.iv())*rit->a1s());
                  break;
                  }

                case evC :
                  {
                  break;
                  }
                default:
                  std::cout << "Operation " << rit->op().c_str() << " not available\n" << std::endl;
                  break;
              } // switch
            } // if !input

          }
        }

    /** \brief Computation of significance value as the width of the product of the value and the adjoint interval */
    void calc_sigval() {
      interval I;
      double sigval;
      for ( auto & it : vec_nodes ) {
        switch ( significance ) {
          case 0 :
            {
              I = abs(it.a1s());
              sigval = I.sup() * it.iv().diam(); //New Significance Definition
              break;
            }
          case 1 :
            {
              I = it.iv() * it.a1s();
              sigval = I.diam();
              break;
            }
          case 2 :
            {
              sigval = it.a1s().diam();
              break;
            }
//            case 3 :
//              {
//                break;
//              }
          default :
            {
              I = abs(it.a1s());
              sigval = I.sup() * it.iv().diam(); //New Significance Definition
              break;
            }
        }
        it.set_sigval(sigval);
      }
    }

    /** \brief Propagation of insignificances: if a node has only insignificance input or outputs itself is insignificant
     * in case that insignificant values are set equal to constants
     */
    void propagate_sig() {
      for ( auto it = vec_nodes.begin(); it != vec_nodes.end(); ++it ) {
        if (it->sig() && !it->_pred.empty() && !it->is_out()) {
          it->set_insig();
          for ( auto & p_it : it->_pred ) { // Check if a node has a significant input
            if (vec_nodes[p_it].sig()) {
              it->set_sig();
            }
          }
        }
      }
      for ( auto rit = vec_nodes.rbegin(); rit != vec_nodes.rend(); ++rit ) {
        if (rit->sig() && !rit->_succ.empty() && !rit->is_out()) {
          rit->set_insig();
          for ( auto & s_it : rit->_succ ) { // Check if a node has a significant output
            if (vec_nodes[s_it].sig()) {
              rit->set_sig();
            }
          }
        }
      }
    }

    /** \brief Comparison of significance value to significance bound */
    void identify_sig( ) {
      for ( auto & it : vec_nodes ) {
//        if (it.is_out()) {
//          it.set_sig();
//        } else {
          if (it.sigval() > _eps) {
            it.set_sig();
          } else {
            it.set_insig();
          }
//        }
      }
    }

    /** \brief Updates the significance bound and returns the number insignificant nodes before and after the propagation of insignificances */
    long update_sig() {
      identify_sig();
      propagate_sig();
      return count_insig();
    }

    void reset() {
      for ( auto & it : vec_nodes ) {
        it.reset();
      }
      _nout = 0;
    }

    void set_eps(double eps) {
      _eps = eps;
    }

    /** \brief Start Analysis with splitting*/
    void analyse(long sid, long start = -1, long last_split = 0) {
      std::cout << "***** START ANALYSIS for Scenario " << sid << " *****\n";
      if (sid == 1) write_code_init();
      if ( start == -1 ) {
        read_scenario(sid, last_split);
        start = last_split;
      }
      //std::cout << "After read scenario\n";
      interpret_value();
      //std::cout << "After value\n";
      interpret_adjoint();
      //std::cout << "After adjoint\n";
      calc_sigval();
      //std::cout << "After sigval\n";
      long insig = update_sig();
      //if (vec_nodes.size() < 100) print_graph(sid);
      write_ranking(sid,insig);
      write_scenario(sid,"finish");
//      if ( /*4*insig < vec_nodes.size() &&*/ last_split < vec_nodes.size() ) { // If less than 25% of the operations are insig
        long vid = -1;
        splitting(sid,last_split,vid); //Find vid for split + change split list + write scenario
        if ( vid != -1 ) {
          if ( last_split != vid ) {
            start++;
          }
          write_code(sid/2,start,vid);
          write_dot(sid/2);
          std::cout << "Scenario " << sid/2 << " finished\n\n";
//          std::cout << "Start analysis for scenario " << sid << " with last split for v" << vid << "\n";
          analyse(sid,vid,vid);
        } else {
          write_code(sid,start,vec_nodes.size());
          write_dot(sid);
          std::cout << "Scenario " << sid << " finished\n\n";
        }
//      } else {
//        write_code(sid,start,vec_nodes.size());
//        write_dot(sid);
//        if (vec_nodes.size() < 300) print_graph(sid);
//        //else print_io();
//      }
    };

    /** \brief Start Analysis */
    void analyse() {
      interpret_value();
      interpret_adjoint();
      calc_sigval();
    }
  } // namespace ia1s
}  // namespace dco

#endif // __DCOS_INTERPRETER__
