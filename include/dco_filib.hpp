#ifndef DCO_FILIB_HPP
#define DCO_FILIB_HPP


#include "interval/interval.hpp"
#include "dco.hpp"

typedef filib::interval<double,filib::native_switched,filib::i_mode_extended> interval;

namespace dco {

  typedef tt1s< interval > it1s;

  typedef ta1sm< interval > ia1s;

// // instantiate (define) global tape pointer
// template <> dco::ta1s<float>::tape *dco::ta1s<float>::global_tape = NULL;

}

#endif // DCO_FILIB_HPP


