#ifndef  __DCO_SCORPIO__
#define  __DCO_SCORPIO__

#include <cmath>
#include <string>
#include <limits>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <map>
#include <vector>
#include <tuple>
#include <queue>
#include <memory>
#include <cfloat>
#include <assert.h>
#include <unistd.h>
#include "interval/interval.hpp"

#define DEBUG

#ifdef __DEBUG__
#define DBG1(a        ) std::cerr << "DBG :: " <<a                                <<std::endl;
#define DBG2(a,b      ) std::cerr << "DBG :: " <<a<<" "<<b                        <<std::endl;
#define DBG3(a,b,c    ) std::cerr << "DBG :: " <<a<<" "<<b<<" "<<c                <<std::endl;
#define DBG4(a,b,c,d  ) std::cerr << "DBG :: " <<a<<" "<<b<<" "<<c<<" "<<d        <<std::endl;
#define DBG5(a,b,c,d,e) std::cerr << "DBG :: " <<a<<" "<<b<<" "<<c<<" "<<d<<" "<<e<<std::endl;
#else
#define DBG1(a        )
#define DBG2(a,b      )
#define DBG3(a,b,c    )
#define DBG4(a,b,c,d  )
#define DBG5(a,b,c,d,e)
#endif

typedef double filib_interval_basetype;
typedef filib::interval<filib_interval_basetype,
       filib::native_switched,
       filib::i_mode_extended> interval;
//typedef filib::interval<filib_interval_basetype> interval;

#define smax std::numeric_limits<std::streamsize>::max()
namespace dco {
  namespace ia1s {
    class node;
    std::vector< node > vec_nodes;  // Vector of nodes
    std::vector< std::tuple<long, double, double> > split_nodes; // Vector of nodes that should be split
    long _ninp = 0, _nint = 0, _nout = 0, _nops = 0;
    long _nsplit = 0;
    double _eps = 0;
    int significance = 0;
    std::map<std::string,int> colors;
    std::map<long,interval> split; // Vector of input intervals and intervals for splitted variables
  }
}

#include "dco_scorpio_node.hpp"
#include "dco_scorpio_type.hpp"
#include "dco_scorpio_interpreter.hpp"

#endif // __DCO_SCORPIO__
