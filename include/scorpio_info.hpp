#ifndef  __SCORPIO_INFO__
#define  __SCORPIO_INFO__

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <memory>

#include "interval/interval.hpp"
#include "dco_scorpio.hpp"

#include "scorpio_info_item.hpp"


// serialization support with cereal 
//   http://uscilab.github.io/cereal/
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>
#include <fstream>

class scorpio_info 
{
private:
  typedef std::vector< scorpio_info_item >  si_container_type;
public:
  typedef si_container_type::iterator   iterator;
  typedef si_container_type::reverse_iterator   reverse_iterator;

public:
  
  //------------------
  // constructors
  //------------------

  // default constructor
  scorpio_info( std::string n = "",  int c = 0 ) : _name(n), _data(c), _sig(0)  {
  }

  scorpio_info( bool tmp, std::string n = "",  int c = 0 ) : _name(n), _data(c), _sig(0)  {
  }

  // copy constructor that re-uses an already allocated tape
  scorpio_info( scorpio_info& si, std::string n = "" ) : 
    _name(n), _data(si._data.size()), _sig(0) {}

  //------------------
  // destructor
  //------------------

  ~scorpio_info() {
    clear();
  }


  //------------------
  // store information
  //------------------
  void add( const scorpio_info_item & si ) {
    _data.push_back( si );

    // dco::ia1s::set_propagated( false );
  }

  void add( dco::ia1s::type& x, std::string n,
	    std::string f, int l,
	    int t1=-1, int t2=-1, int t3=-1, 
      int t4=-1, int t5=-1, int t6=-1, std::string c = "" ) {
    dco::ia1s::register_variable( x, n );
    add( scorpio_info_item(x,n,f,l,t1,t2,t3,t4,t5,t6,c) );
  }

  void add( dco::ia1s::type& x, std::string n,
	    int t1=-1, int t2=-1, int t3=-1, 
      int t4=-1, int t5=-1, int t6=-1, std::string c = "" ) {
    add( x,n,"",-1,t1,t2,t3,t4,t5,t6,c );
  }


  void add( std::vector<dco::ia1s::type>& x, std::string n,
      std::string f, int l,
	    int t1=-1, int t2=-1, int t3=-1, 
      int t4=-1, int t5=-1, int t6=-1, std::string c = "" ) {
    std::vector<dco::ia1s::type>::iterator it;
    int i = 0;
    std::ostringstream is;
    for ( it = x.begin(); it != x.end(); ++it, ++i) {
      std::cerr << "HI  " << *it << std::endl;
      is << i;
      add( *it,n+"["+is.str()+"]",f,l,t1,t2,t3,t4,t5,t6,c );
      };
  }

  void add( std::vector<dco::ia1s::type>& x, std::string n,
	    int t1=-1, int t2=-1, int t3=-1, 
      int t4=-1, int t5=-1, int t6=-1, std::string c = "" ) {
    add( x, n, "", -1, t1, t2, t3, t4, t5, t6, c );
  }

  void register_input( dco::ia1s::type& x, double lb, double ub,
           std::string n, std::string f, int l,
		       int t1=-1, int t2=-1, int t3=-1, 
           int t4=-1, int t5=-1, int t6=-1, std::string c = "" ) {
    //    x = interval( lb, ub );
    dco::ia1s::register_input( x, lb, ub, n );
    // dco::ia1s::register_input( x, lb, ub, n, f,l,t1,t2,t3,t4,t5 );
    add( scorpio_info_item( x, n, f, l, t1, t2, t3, t4, t5, t6, c ) );
  }

  void register_input( dco::ia1s::type& x, double lb, double ub, std::string n,
		       int t1=-1, int t2=-1, int t3=-1, 
           int t4=-1, int t5=-1, int t6=-1, std::string c = "" ) {
    register_input( x, lb, ub, n, "", -1, t1, t2, t3, t4, t5, t6, c );
  }


  // void register_input( dco::ia1s::type& x, std::string n,
  // 		       std::string f, int l,
  // 		       int t1=-1, int t2=-1, int t3=-1, 
  // 		       int t4=-1, int t5=-1, int t6=-1, std::string c = ""
  // 		       )
  // {
  //   if ( ! (*_tp) ) { 
  //     std::cerr << " ERROR:: scorpio_info: register_input()"
  // 		<< " Tape not allocated. var: " << n << std::endl;
  //   }
  //   else 
  //     _tp->register_variable( x );
  //   add( scorpio_info_item( x,n,f,l,t1,t2,t3,t4,t5,t6,c) );
  // };

  // void register_input( dco::ia1s::type& x, std::string n,
  // 		       int t1=-1, int t2=-1, int t3=-1, 
  // 		       int t4=-1, int t5=-1, int t6=-1, std::string c = ""
  // 		       )
  // { 
  //   register_input( x,n,"",-1, t1,t2,t3,t4,t5,t6,c);  
  // }

  // void register_input( std::vector<dco::ia1s::type>& x, std::string n,
  // 		       std::string f, int l,
  // 		       int t1=-1, int t2=-1, int t3=-1, 
  // 		       int t4=-1, int t5=-1, int t6=-1, std::string c = ""
  // 		       )
  // {
  //   std::vector<dco::ia1s::type>::iterator it;
  //   int i = 0;
  //   std::ostringstream is;
  //   for ( it = x.begin(); it != x.end(); ++it, ++i)  {
  //     std::cerr << "HI  " << *it << std::endl;
  //     is << i;
  //     register_input( *it,n+"["+is.str()+"]",f,l,t1,t2,t3,t4,t5,t6,c );
  //     };
  // };
  //
  // void register_input( std::vector<dco::ia1s::type>& x, std::string n,
  // 		       int t1=-1, int t2=-1, int t3=-1, 
  // 		       int t4=-1, int t5=-1, int t6=-1, std::string c = ""
  // 		       )
  // { 
  //   register_input( x,n,"",-1, t1,t2,t3,t4,t5,t6,c);  
  // }

  void register_output( dco::ia1s::type& x, double seed, std::string n,
		       std::string f, int l,
		       int t1=-1, int t2=-1, int t3=-1, 
           int t4=-1, int t5=-1, int t6=-1, std::string c = "" ) {
    dco::ia1s::register_output( x, seed, n );
    add( scorpio_info_item( x, n, f, l, t1, t2, t3, t4, t5, t6, c) );
  }

  void register_output( dco::ia1s::type& x, double seed, std::string n,
		       int t1=-1, int t2=-1, int t3=-1, 
           int t4=-1, int t5=-1, int t6=-1, std::string c = "" ) {
    register_output( x,seed,n,"",-1, t1,t2,t3,t4,t5,t6,c);
  }

  void register_split( dco::ia1s::type& x, double beta, double gamma) {
    dco::ia1s::register_split(x,beta,gamma);
  }

  //------------------
  // getter
  //------------------
  std::string name( void ) const {
    return _name;
  }
  
  scorpio_info_item operator[] ( int n ) const {
    return _data[n];
  }
  
  bool is_propagated( ) const { 
    //    return dco::ia1s::is_propagated();
    return true;
  }

  //------------------
  // STL container interface
  //------------------
  void push_back( const scorpio_info_item & si ) {
    _data.push_back( si );
  }

  int size( void ) const {
    return _data.size();
  }
  
  int capacity() const {
    return _data.capacity(); 
  }
  
  void reserve( int n ) {
    _data.reserve( n );
  }
  
  void resize( int n ) {
    _data.resize( n );
  }

  void clear( void ) {
    _data.clear( );
    // dco::ia1s::clear();
  }
  
  //------------------
  // iterator support 
  //------------------
  std::vector<scorpio_info_item>::iterator begin() {
    return _data.begin();
  }
  std::vector<scorpio_info_item>::iterator end() {
    return _data.end();
  }
  std::vector<scorpio_info_item>::reverse_iterator rbegin() {
    return _data.rbegin();
  }
  std::vector<scorpio_info_item>::reverse_iterator rend() {
    return _data.rend();
  }
  

  //------------------
  //  dco_scorpio  interface
  //------------------

  // Do the final work to get significance information: 
  //   * get adjoints
  //   * compute standard significance
  // This method will propagate adjoints from outputs registered via 
  // register_output through the associated dco_ia1s::tape, and store
  // the adjoints for all inputs and other added variables _data.

  void set_significance(int i) {
    _sig = i;
    dco::ia1s::significance = i;
  }

  void analyse( long sid, double eps) {
    dco::ia1s::set_eps(eps);
    // get adjoints
    dco::ia1s::analyse( sid );
    store_adjoint();
    for ( auto& e : _data )
      e.compute_significance(_sig);
  }

  void analyse() {
    dco::ia1s::analyse( );
    store_adjoint();
    for ( auto& e : _data )
      e.compute_significance(_sig);
  }

  void store_adjoint( ) {
      std::vector<scorpio_info_item>::iterator  r = _data.begin();
      for ( ; r != _data.end(); ++r ) 
        r->store_adjoint();
  }
  
  void reset() {
    dco::ia1s::reset();
  }

//------------------
//  file writer
//------------------

void write_to_file( std::string fname ) {
  std::cout << "# Scorpio_Info write_to_file "<<fname<<":: " //<< name() 
    << "  N:" << size()  << " C:" << capacity() << std::endl;
  std::ofstream file( fname, std::ios::binary );
  cereal::BinaryOutputArchive oarchive(file); // Create an output archive
  oarchive(_data); // Write the data to the archive
}


//------------------
//  file reader
//------------------

void read_from_file( std::string fname ) {
  _data.clear( );
  std::ifstream file( fname, std::ios::binary );
  cereal::BinaryInputArchive iarchive(file); // Create an input archive
  iarchive(_data); // Read the data from the archive
  std::cout << "# Scorpio_Info read_from_file "<<fname<<":: " //<< name() 
	    << "  N:" << size()  << " C:" << capacity() << std::endl;
}

void graph( double eps = 0, std::string fname="graph" ) {
  dco::ia1s::set_eps(eps);
  dco::ia1s::identify_sig();
  dco::ia1s::write_dot(0,fname);
}

void code( double eps = 0, std::string fname="f" ) {
  dco::ia1s::set_eps(eps);
  dco::ia1s::identify_sig();
  dco::ia1s::write_code_init(fname);
  dco::ia1s::write_code(fname);
}

private :
  std::string                 _name;
  int                         _sig;
  si_container_type           _data;


}; // class scorpio_info
  

//------------------
//  stream output operator 
//------------------

std::ostream& operator<<( std::ostream &s, scorpio_info &sis) {
  int i = 0;
  s << "Scorpio_Info :: " << sis.name() 
    << "  N:" << sis.size()  << " C:" << sis.capacity() << std::endl;
  std::vector<scorpio_info_item>::iterator  r = sis.begin();
  for ( ; r != sis.end(); ++r, ++i ) 
    s << std::setw(3) << i << ": " << *r << std::endl;
  return s;
}


//------------------
//  pre-processor macros that allow automatic gnereation of
//    location information ( file name and line number )
//   Must be placed in the corresponding soiurce code line!!
//------------------

//  helper macros converting macro args to strungs
#define STR(a) #a
#define STR2(a,n) #a " " n

//  Macro to ADD new scorpio_info_item to scorpio_info - instance s
//#define SIADDM( s,x, ... )  s.add( scorpio_info_item( __FILE__, __LINE__, x,STR(x), __VA_ARGS__ ) );
//#define SIADDMD( s,x,n, ... )  s.add( scorpio_info_item( __FILE__, __LINE__, x,STR2(x,n), __VA_ARGS__ ) );
#define SIADDM( s,x, ... )  s.add( x, "", -1, __VA_ARGS__ );
#define SIADDMD( s,x,n, ... )  s.add( x, n, __FILE__, __LINE__, __VA_ARGS__ );
#define INTERMEDIATE( s,x,n, ... )  s.add( x, n, __FILE__, __LINE__, __VA_ARGS__ );

//  Macro to register an INPut variable  to scorpio_info - instance s
#define SIINPM( s,x,l,u, ... )  s.register_input( x, l,u, STR(x), __FILE__, __LINE__, __VA_ARGS__ );
#define SIINPMD( s,x,l,u,n, ... )  s.register_input( x,l,u,STR2(x,n),  __FILE__, __LINE__, __VA_ARGS__ );
#define INPUT( s,x,l,u,n, ... )  s.register_input( x,l,u,STR2(x,n),  __FILE__, __LINE__, __VA_ARGS__ );

//  Macro to register an OUTput variable  to scorpio_info - instance s
#define SIOUTM( s,x,sd, ... ) s.register_output( x, sd, STR(x), __FILE__, __LINE__, __VA_ARGS__ );
#define SIOUTMD( s,x,sd,n, ... ) s.register_output( x, sd, STR2(x,n), __FILE__, __LINE__, __VA_ARGS__ );
#define OUTPUT( s,x,sd,n, ... ) s.register_output( x, sd, STR2(x,n), __FILE__, __LINE__, __VA_ARGS__ );

#endif // __SCORPIO_INFO__
