#!/bin/bash

mkdir -p data/analyse data/finish data/code data/graph data/rank
rm -f ./data/analyse/* ./data/finish/* ./data/code/* ./data/graph/graph* ./data/rank/rank*
#cp ./data/scenario1.txt ./data/analyse/
touch ./data/analyse/scenario1.txt
eps=0.5
while [ "$(ls -A ./data/analyse)" ]
do
    for file in ./data/analyse/* 
    do  
        #cp $file ./data/test/
        fname=$(basename "$file")
        sid=$(echo $fname | sed 's/[^0-9]//g')
        echo ./iasa $eps $sid
        ./iasa $eps $sid
        #ls ./data/*/*
        rm $file
    done
done

for file in ./data/code/*.cpp
do 
    fname=$(basename "$file" .cpp)
    echo "void $fname();" >> ./data/code/f.h
done
