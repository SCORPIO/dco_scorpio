#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>

// make flattener available
#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

template <typename T> void f(T &x, T &y) {
  y = 0.5 * sin(x) + exp(x/2);
}

int main(int argc, char *argv[])
{
  int sid;
  double eps;
  if ( argc == 3 ) {
    eps = atof(argv[1]);
    sid = atoi(argv[2]);
  } else if ( argc == 2 ) {
    eps = atof(argv[1]);
  } else {
    return 0;
  }
  scorpio_info si1(argv[0]);
  // declare variables
  dco::ia1s::type x, y;

  // input registration
  SIINPMD(si1, x, -5, 5, "in", -1, 1 );
  si1.register_split(x,2.5,10);

  // computation
  f(x,y);

  // output registration
  SIOUTMD(si1, y, 1.0, "out", -1, 3 );

  if ( argc == 2 ) si1.analyse();
  if ( argc == 3 ) si1.analyse(sid,eps); 
  std::cout << si1 << std::endl;
  //si1.graph(0.5,argv[0]);
  return 0;
}
