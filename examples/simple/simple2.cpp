#include <iostream>
#include <string>
#include <cmath>

// make flattener available
#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

int main(int argc, char *argv[])
{
  double eps = 0.5;
  scorpio_info si1("simple");
  // declare inputs
  dco::ia1s::type x;
  double cx(5), rx(5);

  // input registration
  SIINPMD(si1, x, 2.5, 5, "in", -1, 1 );

  // computation
  dco::ia1s::set_section("1");
  dco::ia1s::type t1 = exp(x/2);
  dco::ia1s::set_section("2");
  dco::ia1s::type t2 = 0.5*sin(x);
  dco::ia1s::set_section("3");
  dco::ia1s::type y = t1 + t2;

  // output registration
  SIOUTMD(si1, y, 1.0, "out",-1,3 );
	
	si1.set_significance(1);
  si1.analyse();
	std::cout << si1 << std::endl;
  si1.graph(eps,"simple2");
  return 0;
}
