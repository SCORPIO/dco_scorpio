#include <iostream>
#include <string>
#include <cmath>
#include<vector>

// make flattener available
#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

int main(int argc, char *argv[])
{
  double eps;
  if ( argc == 2 ) {
    eps = atof(argv[1]);
  } else {
    std::cout<<"Call: ./iasa eps\n";
    return 0;
  }
  //std::cout << "SID: " << sid << std::endl;
  scorpio_info si1("simple1");

  // declare variables
	std::vector<dco::ia1s::type> x(2);
  dco::ia1s::type y;

  x[0] = 2;
	x[1] = 4;
  // input registration
  INPUT(si1, x[0], 3, 4, "Input x0", -1, 1 );
  INPUT(si1, x[1], 1, 2, "Input x1", -1, 1 );

  // computation
  // The comparison is applied to the floating point computation with x[0]=2 and x[1]=4
  // The interval ranges in INPUTD is irrelevant for the comparison 
	if (x[0] > x[1])  
		y = x[0];
	else
		y = x[1];

  // output registration
  OUTPUT(si1, y, 1.0, "out",-1,3 );

  si1.analyse();
  si1.graph(eps,"simple4");
  si1.code(eps,"code");

  std::cout << si1 << std::endl;
  return 0;
}
