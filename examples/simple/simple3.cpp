#include <iostream>
#include <string>
#include <cmath>

// make flattener available
#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

int main(int argc, char *argv[])
{
  double eps;
  if ( argc == 2 ) {
    eps = atof(argv[1]);
  } else {
    eps = 0;
  }
  //std::cout << "SID: " << sid << std::endl;
  scorpio_info si1("simple3");

  // declare inputs
  dco::ia1s::type x,z;
  double cx(3),   rx(2.5);
  // declare output

  // input registration
  INPUT(si1, x, cx-rx, cx+rx, "Input", -1, 1 );
  INPUT(si1, z, cx, cx+2*rx, "Input", -1, 1 );

  // computation
  dco::ia1s::type y;
  y = max(x,z);
  INTERMEDIATE(si1,y,"max",-1,2);
  y = min(x,z);
  INTERMEDIATE(si1,y,"min",-1,2);
  y = round(x);
  INTERMEDIATE(si1,y,"round",-1,2);
  y = ceil(x);
  INTERMEDIATE(si1,y,"ceil",-1,2);
  y = floor(x);
  INTERMEDIATE(si1,y,"floor",-1,2);
  y = fabs(x);
  INTERMEDIATE(si1,y,"fabs",-1,2);
  y = abs(x);
  INTERMEDIATE(si1,y,"abs",-1,2);
  y = x*z;
  INTERMEDIATE(si1,y,"*",-1,2);
  y = log(x);
  INTERMEDIATE(si1,y,"log",-1,2);
  y = log10(x);
  INTERMEDIATE(si1,y,"log10",-1,2);

  // output registration
  OUTPUT(si1, y, 1.0, "out",-1,3 );

  si1.analyse();

  std::cout << si1 << std::endl;

  si1.graph(eps,"simple3");
  si1.code(eps,"code");
  return 0;
}
