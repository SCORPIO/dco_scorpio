#include <iostream>
#include <string>
#include <cmath>

// make flattener available
#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

int main(int argc, char *argv[])
{
  double eps;
  if ( argc == 2 ) {
    eps = atof(argv[1]);
  } else {
    std::cout<<"Call: ./iasa eps\n";
    return 0;
  }
  //std::cout << "SID: " << sid << std::endl;
  scorpio_info si1("simple1");

  // declare variables
  dco::ia1s::type x, a, b, c, d, e, f, g;

  // input registration
  SIINPMD(si1, x, 0.25, 4.75, "Input", -1, 1 );

  // computation
  a = x/2;
  SIADDMD(si1,a,"a",-1,2);
  b = sin(a); 
  SIADDMD(si1,b,"b",-1,2);
  c = cos(a);
  SIADDMD(si1,c,"c",-1,2);
  d = a*2;
  SIADDMD(si1,d,"d",-1,2);
  e = d+2;
  SIADDMD(si1,e,"e",-1,2);
  f = c-2;
  SIADDMD(si1,f,"f",-1,2);
  b -= a;

  dco::ia1s::type y = a+b+c+d+e+f+g;

  // output registration
  SIOUTMD(si1, y, 1.0, "out",-1,3 );

  si1.analyse();
  si1.graph(eps,"simple1");
  si1.code(eps,"code");

  std::cout << si1 << std::endl;
  return 0;
}
