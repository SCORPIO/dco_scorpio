#include <cmath>

// make flattener available
#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

template <typename T> void f(T &x, T &y) {
  y = 0.5 * sin(x) + exp(x/2);
}

int main(int argc, char *argv[])
{
  scorpio_info si1(argv[0]);
  // declare variables
  dco::ia1s::type x, y;

  // input registration
  SIINPMD(si1, x, -5, 5, "in", -1, 1 );

  // computation
  f(x,y);

  // output registration
  SIOUTMD(si1, y, 1.0, "out", -1, 3 );
  
  si1.set_significance(0);
  si1.analyse();
  si1.graph(0.5);
  si1.code(0.5);
  si1.code(1,"f2");
  return 0;
}
