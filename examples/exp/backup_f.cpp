#include <vector>
#include <cmath>
#include <iostream>
void f0();
void f1();
double v0,  v1,  v2;
void f(std::vector<double>& x, std::vector<double>& y) {
  v0 = x[0];
  if ( v0 >= -16 && v0 <= 16) {
    f1();
  } else {
    f0();
  }
  y[0] = v2 ;
}

int main(int argc, char *argv[]){
  std::vector<double> x(1), y(1);
	if ( argc == 2 ) {
		x[0] = std::atof(argv[1]);
  } else {
		x[0] = 0;
  }
  f(x,y);
  std::cout << "y =";
  for ( auto & it : y ) {
    std::cout << ' ' << it << ' ' << exp(x[0]);
    std::cout << "\n";
  }
}
