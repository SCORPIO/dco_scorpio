#include <iostream>
#include <string>
#include <cmath>
#include<cstdlib>

// make flattener available
#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

int main(int argc, char *argv[])
{
  long sid;
  double eps;
  if ( argc == 3 ) {
    eps = atof(argv[1]);
		sid = atoi(argv[2]);
  } else if ( argc == 2) {
    eps = atof(argv[1]);
	} else {
    std::cout<<"Call: ./iasa eps [sid]\n";
    return 0;
  }
  scorpio_info si1("simple1");

  // declare variables
  dco::ia1s::type x, y;

  // input registration
  SIINPMD(si1, x, -16, 16, "Input", -1, 1 );
  si1.register_split(x, 1, 100);
  // computation
  y = exp(x);

  // output registration
  SIOUTMD(si1, y, 1.0, "out",-1,3 );

	if( argc == 2 ) si1.analyse();
  if( argc == 3 ) si1.analyse(sid,eps);

	//si1.code(5);
  std::cout << si1 << std::endl;
  return 0;
}
