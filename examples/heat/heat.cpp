#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"
#include <cstdlib>

#include <iomanip>
using std::setw;
#include <iostream>
using std::cout; using std::cerr; using std::endl; using std::flush;
#include <vector>
using std::vector;
#include <fstream>
using std::ofstream; using std::ifstream;
#include <cmath>
using std::sqrt; using std::abs;
#include <algorithm>
using std::max; using std::min; using std::min_element;
#include <limits>

template<typename Type>
void heat(int m, int n, Type& c, Type& x, Type& t, vector<Type>& T) {
  dco::ia1s::set_section("Jac");
  vector<Type> A(n*n);
  //for (int i=0; i<n*n; i++) {
  //  A[i] = 0;
  //}
  A[0] = 1;
  A[n*n-1] = 1;
  Type aux = t*pow(n-1,2.0)*c/(m*pow(x,2.0));
  for (int i=1; i<n-1; i++) {
    A[i*n+i-1] = -aux;
    A[i*n+i] = 2*aux+1;
    A[i*n+i+1] = -aux;
  }

  dco::ia1s::set_section("LU");
  for (int k=0; k<n; k++) {
    for (int i=k+1; i<n; i++) {
      //if (A[i*n+k] != 0) {
        A[i*n+k] /= A[k*n+k];
      //}
    }
    for (int j=k+1; j<n; j++) {
      for (int i=k+1; i<n; i++) {
        //if (A[i*n+k] != 0 && A[k*n+j] != 0) {
          //if(A[i*n+j] == 0)
            //A[i*n+j] = -A[i*n+k]*A[k*n+j];
          //else
            A[i*n+j] -= A[i*n+k]*A[k*n+j];
        //}
      }
    }
  }

  for (int j=0; j<m; ++j) {
    dco::ia1s::set_section("FS");
    for (int i=0; i<n; i++) {
      for (int j=0; j<i; j++) {
        //if (A[i*n+j] != 0) {
          T[i] -= A[i*n+j]*T[j];
        //}
      }
    }

    dco::ia1s::set_section("BS");
    for (int i=n-1; i>=0; i--) {
      for (int j=n-1; j>i; j--) {
        //if (A[i*n+j] != 0) {
          T[i] -= A[i*n+j]*T[j];
        //}
      }
      T[i] /= A[i*n+i];
    }
  }
}

template<typename Type>
void sparse_heat(int m, int n, Type& c, Type& x, Type& t, vector<Type>& T) {
  dco::ia1s::set_section("Jac");
  vector<Type> A(3*n);
  A[0] = 0;
  A[1] = 1;
  A[2] = 0;
  Type a = t*pow(n-1,2.0)*c/(m*pow(x,2.0));
  for (int i=1; i<n-1; i++) {
    A[i*3] = -a;
    A[i*3+1] = 2*a+1;
    A[i*3+2] = -a;
  }
  A[3*n-3] = 0;
  A[3*n-2] = 1;
  A[3*n-1] = 0;
  dco::ia1s::set_section("LU");
  for (int i=1; i<n; i++) {
    A[i*3] /= A[i*3-2];
    A[i*3+1] -= A[i*3]*A[i*3-1];
  }

  for (int j=0; j<m; ++j) {
    dco::ia1s::set_section("FS");
    for (int i=1; i<n; i++) {
      T[i] -= A[i*3]*T[i-1];
    }

    dco::ia1s::set_section("BS");
    T[n-1] /= A[3*n-2];
    for (int i=n-2; i>=0; i--) {
      T[i] -= A[i*3+2]*T[i+1];
      T[i] /= A[i*3+1];
    }
  }
}

int main(int argc, char* argv[]){
  cout.precision(15);
  long sid;
  double eps;
  if ( argc == 2 ) {
    eps = atof(argv[1]);
    sid = 1;
  } else if ( argc == 3 ) {
    eps = atof(argv[1]);
    sid = atol(argv[2]);
  } else {
    std::cout<<"Call: ./iasa eps [optional: scenario ID]\n";
		return 0;
  }
  int n=41, m=800;
  double pc = 0.01, pT0 = 300, pTN = 1700, L = 2;
  double t_s = 0, t_f = 32;

  scorpio_info si1("Heat");

  dco::ia1s::set_section("IO");
  dco::ia1s::type c = pc;
  dco::ia1s::type x = L;
  dco::ia1s::type t = t_f;
  dco::ia1s::type T0 = pT0;
  dco::ia1s::type TN = pTN;
  SIINPMD( si1, t, t_s, t_f, "Input", -1, 1 );
  si1.register_split( t, 1, DBL_MAX );
  SIINPMD( si1, TN, 1695, 1705, "Input", -1, 1 );
  SIINPMD( si1, T0, 295, 305, "Input", -1, 1 );

  vector<dco::ia1s::type> T(n);
  for (int i=0; i<n-1; i++) T[i] = T0;
  T[n-1] = TN;

  sparse_heat(m,n,c,x,t,T);
  //heat(m,n,c,x,t,T);
  
  dco::ia1s::set_section("IO");
  SIOUTMD( si1, T[n/2], 1, "out",-1,3 );
  si1.analyse(sid, eps);

  return 0;
}
