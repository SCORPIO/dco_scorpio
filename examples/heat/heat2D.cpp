#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

typedef dco::ia1s::type type_t;

scorpio_info si1("Heat");

#define NTASK 10
int m  = 50;
int nx = 41;
int ny = 21;
int nn;

void print(int t, type_t *T) {
  FILE *fp;
  int i, j;
  char t_string[10], fname[32] = "data/";
  double temp;

  sprintf(t_string, "%d", t);
  strcat(fname, t_string);
  strcat(fname, ".dat");
  fp = fopen(fname, "w");

  for(i=0; i<ny; i++) {
    for(j=0; j<nx; j++) {
      //dco_value(&temp, T[i+j*ny], 0);
      //fprintf(fp, "%f ", temp);
      fprintf(fp, "%f ", T[i+j*ny].value());
    }
    fprintf(fp, "\n");
  }
}

void setup_system_matrix(type_t c, type_t l, type_t t, type_t* A, type_t* B) {
  int i, j;
  type_t sx, sy, dt, dx, dy;
  dt = t/(2*m);
  dx = l/(nx-1);
  dy = l/(ny-1);
  sx = c*dt/(dx*dx);
  sy = c*dt/(dy*dy);

  // Matrix A for Early Step
  // Boundary Condition at x=0
  for (i=0; i<ny; i++) {
    A[3*i] = 0;
    A[3*i+1] = 1;
    A[3*i+2] = 0;
  }
  for (i=1; i<nx-1; i++) {
    // Boundary Condition at y=0
    A[3*i*ny] = 0;
    A[3*i*ny+1] = 1;
    A[3*i*ny+2] = 0;
    for (j=1; j<ny-1; j++) {
      A[3*(i*ny+j)] = -sy;
      A[3*(i*ny+j)+1] = 2*sy+1;
      A[3*(i*ny+j)+2] = -sy;
    }
    // Boundary Condition at y=ly
    A[3*(i+1)*ny-3] = 0;
    A[3*(i+1)*ny-2] = 1;
    A[3*(i+1)*ny-1] = 0;
  }
  // Boundary Condition at x=lx
  for (i=0; i<ny; i++) {
    A[3*((nx-1)*ny+i)] = 0;
    A[3*((nx-1)*ny+i)+1] = 1;
    A[3*((nx-1)*ny+i)+2] = 0;
  }
  // Prepare for Thomas algorithm
  A[2] /=  A[1];
  for (i=1; i<nn-1; i++) {
    A[3*i+2] /= (A[3*i+1] - A[3*i]*A[3*i-1]);
  }

  // Matrix B for Late Step
  // Boundary Condition at y=0
  for (j=0; j<nx; j++) {
    B[3*j] = 0;
    B[3*j+1] = 1;
    B[3*j+2] = 0;
  }
  for (i=1; i<ny-1; i++) {
    // Boundary Condition at x=0
    B[3*i*nx] = 0;
    B[3*i*nx+1] = 1;
    B[3*i*nx+2] = 0;
    for (j=1; j<nx-1; j++) {
      B[3*(i*nx+j)] = -sx;
      B[3*(i*nx+j)+1] = 2*sx+1;
      B[3*(i*nx+j)+2] = -sx;
    }
    // Boundary Condition at x=lx
    B[3*(i+1)*nx-3] = 0;
    B[3*(i+1)*nx-2] = 1;
    B[3*(i+1)*nx-1] = 0;
  }
  // Boundary Condition at y=ly
  for (j=0; j<nx; j++) {
    B[3*((ny-1)*nx+j)] = 0;
    B[3*((ny-1)*nx+j)+1] = 1;
    B[3*((ny-1)*nx+j)+2] = 0;
  }
  // Prepare for Thomas algorithm
  B[2] /=  B[1];
  for (i=1; i<nn-1; i++) {
    B[3*i+2] /= (B[3*i+1] - B[3*i]*B[3*i-1]);
  }
}

void setup_rhs_e(int k,  type_t *_s, type_t *b, type_t *T) {
  int i, j, step, start, end;
  type_t s = *_s;

  step = ceil((nx-2)/(double)NTASK);
  start = k*step+1;
  end = (k+1)*step+1;
  end = end > nx-1 ? nx-1 : end;
  for (i=start; i<end; i++) {
    b[i*ny] = T[i*ny];
    for (j=1; j<ny-1; j++) {
      b[i*ny+j] = T[i*ny+j]*(1-2*s)+s*(T[(i+1)*ny+j]+T[(i-1)*ny+j]);
    }
    b[(i+1)*ny-1] = T[(i+1)*ny-1];
  }
}

void setup_rhs_l(int k, type_t *_s, type_t *b, type_t *T) {
  type_t s = *_s;
  int i, j, step, start, end;
  step = ceil((nx-2)/(double)NTASK);
  start = k*step+1;
  end = (k+1)*step+1;
  end = end > nx-1 ? nx-1 : end;
  for (i=start; i<end; i++) {
    b[i*ny] = T[i*ny];
    for (j=1; j<ny-1; j++) {
      b[i*ny+j] = T[i*ny+j]*(1-2*s)+s*(T[i*ny+j+1]+T[i*ny+j-1]);
    }
    b[(i+1)*ny-1] = T[(i+1)*ny-1];
  }
}

void generate_rhs_early(type_t c, type_t l, type_t t, type_t *b, type_t *T) {
  int i, j, k;
  type_t s, dt, dx;
  dt = t/(2*m);
  dx = l/(nx-1);
  s = c*dt/(dx*dx);

  for (j=0; j<ny; j++) {
    b[j] = T[j];
  }
  type_t *_s = &s;
  for (k=0; k < NTASK; k++) {
    setup_rhs_e(k,_s,b,T);
  }
  for (j=0; j<ny; j++) {
    b[(nx-1)*ny+j] = T[(nx-1)*ny+j];
  }
}

void generate_rhs_late(type_t c, type_t l, type_t t, type_t *b, type_t *T) {
  int i, j, k;
  type_t s, dt, dy;
  dt = t/(2*m);
  dy = l/(ny-1);
  s = c*dt/(dy*dy);

  for (j=0; j<ny; j++) {
    b[j] = T[j];
  }
  type_t *_s = &s;
  for (k=0; k < NTASK; k++) {
    setup_rhs_l(k,_s,b,T);
  }
  for (j=0; j<ny; j++) {
    b[(nx-1)*ny+j] = T[(nx-1)*ny+j];
  }
}

void early_step(type_t *A, type_t *b, type_t *T) {
  int i, j;
  // Forward Sweep of Thomas Algorithm
  b[0] /= A[1];
  for (i=1; i<nn; i++) {
    b[i] = (b[i]-A[3*i]*b[i-1])/(A[3*i+1]-A[3*i]*A[3*i-1]);
  }
  // Backward Sweep of Thomas Algorithm
  T[nn-1] = b[nn-1];
  for (i=nn-2; i>=0; i--) {
    T[i] = b[i] - A[3*i+2]*T[i+1];
  }
}

void late_step(type_t *B, type_t *b, type_t *T) {
  int i, j;
  // Forward Sweep of Thomas Algorithm
  b[0] /= B[1];
  for (i=1; i<nn; i++) {
    b[i%nx*ny+i/nx] = (b[i%nx*ny+i/nx]-B[3*i]*b[(i-1)%nx*ny+(i-1)/nx])/(B[3*i+1]-B[3*i]*B[3*i-1]);
  }
  // Backward Sweep of Thomas Algorithm
  T[nn-1] = b[nn-1];
  for (i=nn-2; i>=0; i--) {
    T[i%nx*ny+i/nx] = b[i%nx*ny+i/nx] - B[3*i+2]*T[(i+1)%nx*ny+(i+1)/nx];
  }
}

void heat(type_t c, type_t lx, type_t ly, type_t t, type_t* A, type_t* B, type_t *b, type_t* T) {
  int k;

  setup_system_matrix(c,ly,t,A,B);
  for (k=0; k<m; ++k) {
    // Early Step
		generate_rhs_early(c,lx,t,b,T);
    early_step(A,b,T);
    for (int i=0; i<nx*ny; i++) INTERMEDIATE(si1, T[i], "T[i]", -1, 2, k, 0, i);
    // Late Step
    generate_rhs_late(c,ly,t,b,T);
    late_step(B,b,T);
    for (int i=0; i<nx*ny; i++) INTERMEDIATE(si1, T[i], "T[i]", -1, 2, k, 1, i);
    //print(k+1,T);
    continue;
  }
}

int main(int argc, char* argv[]){
  int i, j, threads = 1;
  double pc = 0.01, pT0 = 0, pTN = 1, Lx = 2, Ly = 1;
  double t_f = 10;
  type_t res, c = pc, lx = Lx, ly = Ly, t = t_f, T0 = pT0, TN = pTN;
  type_t *T, *A, *B, *b;

  nn=nx*ny;
  SIINPMD(si1, t, 0, 1, "Input", -1, 1 );
  SIINPMD(si1, TN, 1650, 1700, "Input", -1, 1 );
  SIINPMD(si1, T0, 280, 300, "Input", -1, 1 );
  T = new type_t[nx*ny];
  // IC
  for (i=0; i<nx*ny; i++) T[i] = T0;
  for (i=0; i<nx-1; i++) {
  // BC top T[i*ny]
    T[i*ny] = T0+(TN-T0)/2*i/(ny-1);
  // BC bottom T[(i+1)*ny-1]
    T[(i+1)*ny-1] = T0+(TN-T0)/2*i/(ny-1);
  }
  for (i=0; i<ny; i++) {
  // BC left T[i]
    T[i] = T0;
  //BC right
    T[(nx-1)*ny+i] = TN;
  }

  A = new type_t[3*nx*ny];
  B = new type_t[3*nx*ny];
  b = new type_t[nx*ny];
  //print(0,T);
  heat(c,lx,ly,t,A,B,b,T);

  res = 0;
  for (i=0; i<nn; i++) {
    res += T[i];
  }
  SIOUTMD(si1, res, 1, "out",-1,3 );
  si1.analyse();
	si1.write_to_file("significance.dat");
  //std::cout << si1 << std::endl;
  return 0;
}

