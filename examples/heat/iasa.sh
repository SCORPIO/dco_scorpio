#!/bin/bash

rm ./data/analyse/*
rm ./data/finish/*
rm ./data/code/*.cpp
rm ./data/code/*.h
rm ./data/code/*.o
rm ./data/code/f
rm ./data/graph/graph*
rm ./data/rank/rank*
#cp ./data/scenario1.txt ./data/analyse/
touch ./data/analyse/scenario1.txt
eps=1
while [ "$(ls -A ./data/analyse)" ]
do
    for file in ./data/analyse/* 
    do  
        #cp $file ./data/test/
        fname=$(basename "$file")
        sid=$(echo $fname | sed 's/[^0-9]//g')
        echo ./iasa 1 $sid
        ./iasa $eps $sid
        #ls ./data/*/*
        rm $file
    done
done

for file in ./data/code/*.cpp
do 
    fname=$(basename "$file" .cpp)
    echo "void $fname();" >> ./data/code/f.h
done
