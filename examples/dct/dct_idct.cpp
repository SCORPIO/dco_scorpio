#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>


#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

using namespace std;

//typedef double AD_TYPE;
typedef dco::ia1s::type AD_TYPE;

#define N 128

/* Image Compression: Encoder (DCT+Quantization) and Decoder (Dequantization+IDCT). 
They can be treated as two different kernels, but in order to quantify any error in encoder the quantized DCT output need to be processed by IDCT 

Code prepared as an application benchmark for the SCORPIO project
contact: georgios.karakonstantis@epfl.ch 

*/
 
double COS[8][8], C[8];
unsigned char passive_pic[N][N];
unsigned char pic[N][N];
AD_TYPE dct[N][N], idct[N][N];
AD_TYPE PSNR, MSE;

void sdct_out(scorpio_info &sivar){
  ofstream ofs;
  ofs.open ("sig.txt");
  ofs << scientific;
  for(int i = 0; i < sivar.size(); i++){
    if(sivar[i].tsk1() == 0) {
      if((i+1)%N != 0)
        ofs << sivar[i].significance() << ", ";
      else
        ofs << sivar[i].significance() << ";\n";
    }
  }
  ofs.close();
}
  
/* Calculation of the dct/idct coefficients */ 
void init_coef() {
  int i, j;
  for (i = 0; i < 8; i++) {
    for (j = 0; j < 8; j++)
      COS[i][j] = cos((2 * i + 1) * j * acos(-1) / 16.0);
    if (i) C[i] = 1;
    else C[i] = 1 / sqrt(2);
  }
}
 
 
/*
 DCT coefficients are being applied on 8x8 blocks of an image, in this case a 512*512 image is used so there are 64 blocks in horizontal and vertical direction.
The Significant outputs (low frequencies) are being stored at the upper left corner of each 8x8 block, meaning by for instance that output values within 1x1, 2x2 are more significant 
than 4x4 which are more significant than 6x6 etc. Among those the 1x1 is known as the DC component of each block of the image that is extremely significant;
any error in the operations that contribute to its computation will result in drastic quality loss 
while the rest 63 outputs are known as AC or high frequencies and any approximation in their computations  (mainly in those of the low right corner) will have minor impact on output quality. 
 */
 
void DCT() {
  int r, c, i, j, x, y;
  for (r = 0; r < N/8; r++)
    for (c = 0; c < N/8; c++)
      for (i = 0; i < 8; i++)
        for (j = 0; j < 8; j++) {
          AD_TYPE sum = 0;
          for (x = 0; x < 8; x++)
            for (y = 0; y < 8; y++)
              sum = sum + (pic[r * 8 + x][c * 8 + y] - 128) * COS[x][i] * COS[y][j];
          sum = sum * C[i] * C[j] * 0.25;
          dct[r * 8 + i][c * 8 + j] = sum;
        }
}

/* 
IDCT is being applied on each 8x8 output DCT block in order to decompress the image     
Again the upper left corners are more significant than the lower right ones
 */

void IDCT() {
  int r, c, i, j, x, y;
  for (r = 0; r < N/8; r++)
    for (c = 0; c < N/8; c++)
      for (i = 0; i < 8; i++)
        for (j = 0; j < 8; j++) {
          AD_TYPE sum = 0;
          for (x = 0; x < 8; x++)
            for (y = 0; y < 8; y++)
              sum = sum + C[x] * C[y] * dct[r * 8 + x][c * 8 + y] * COS[i][x] * COS[j][y];
          sum = sum * 0.25;
          sum = sum + 128;
          idct[r * 8 + i][c * 8 + j] = sum;
        }
  FILE * pFile;
  pFile = fopen("output/lenna_out.raw","w");
  for (r = 0; r < N; r++)
    for (c = 0; c < N; c++)
      fputc(idct[r][c].value(),pFile);
      //fputc(idct[r][c],pFile);
  fclose(pFile);
}
 
 /*
 This is the lossy part of the compression that indicates also the weights/significance of each DCT output. 
 The ouputs of each 8x8 dct block are being quantized based on quantization matrices 
 (the matrix used here is based on the JPEG standard, various others can be used based on the required quality). 
 A small value/weight (closer to 1) in the matrix indicates more significant data. Someone can experiment 
 with the values/weights in order to see the impact on quality.

 This part actually approximates the resulted dct outputs trying to ensure high quality (no approximation) of the upper left corners
 of each 8x8 dct block and reduce the values in the lower right corners which are not so necessary/significant. 
    
 After quantization the resulted values are being dequantized in order to be fed to the IDCT for decompression 
  */

void quantization() {
 
 int table[8][8] = {
    {16, 11, 10, 16, 24, 40, 51, 61 },
    {12, 12, 14, 19, 26, 58, 60, 55 } ,
    {14, 13, 16, 24, 40, 57, 69, 56 },
    {14, 17, 22, 29, 51, 87, 80, 82 },
    {18, 22, 37, 56, 68, 109, 103, 77 },
    {24, 35, 55, 64, 81, 104, 113, 92 },
    {49, 64, 78, 87, 103, 121, 120, 101},
    {72, 92, 95, 98, 112, 100, 103, 99}
  };
 
 int r, c, i, j;
 for (r = 0; r < N/8; r++)
   for (c = 0; c < N/8; c++)
     for (i = 0; i < 8; i++)
       for (j = 0; j < 8; j++) {
         dct[r * 8 + i][c * 8 + j] = dct[r * 8 + i][c * 8 + j] / table[i][j];
         dct[r * 8 + i][c * 8 + j] = round(dct[r * 8 + i][c * 8 + j]);
         dct[r * 8 + i][c * 8 + j] = dct[r * 8 + i][c * 8 + j] * table[i][j];
       }
}
 
 /*
 Next the mean square error and peak signal to noise ratio between the original and compressed-decompressed image 
 are calculated. They are not part of the application but are used here for quantifiying the resulted image.
 PSNR around 35db or greater indicates usually perfect quality   
  */

void MSE_PSNR() {
 
  MSE = 0;
  int r, c;
  for (r = 0; r < N; r++)
    for (c = 0; c < N; c++) {
      MSE += pow(pic[r][c] - idct[r][c],2);
    }

  MSE = MSE/(N*N);
  //PSNR = 10 * log10(255 * 255 / MSE);
  //ofstream ofs;
  //ofs.open ("PSNR.txt");
  //ofs << PSNR << endl;
  //ofs.close();
}
 
 /*peppers512.raw and lena512.raw are provided and can be tested*/

int main(int argc, char* argv[]) {

  //  intitialize interval library
  //filib::fp_traits<double>::setup();

  // create a scorpio_info instance with internal tape creation
  scorpio_info si1("Sig Ana of dct");
  freopen("images/lenna512.raw", "r", stdin);
  unsigned char dummy;
  int r, c;
  for (r = 0; r < N; r++) {
    for (c = 0; c < N; c++) {
      scanf("%c", &passive_pic[r][c]);
    }
    for (; c < 512; c++) {
      scanf("%c", &dummy);
    }
  }

  double h = 20;
  for (r = 0; r < N; r++)
    for (c = 0; c < N; c++) {
      pic[r][c] = passive_pic[r][c];
      //SIINPMD(si1,pic[r][c],passive_pic[r][c]-h/2,passive_pic[r][c]+h/2,"input",0,r,c);
      //SIINPMD(si1,dct[r][c],dct[r][c]-h/2,dct[r][c]+h/2,"input",0,r,c);
    }
  init_coef();
  DCT();
  for (r = 0; r < N; r++)
    for (c = 0; c < N; c++) {
      //SIINPMD(si1,dct[r][c],dct[r][c]-h/2,dct[r][c]+h/2,"input",0,r,c);
      SIINPMD(si1,dct[r][c],dct[r][c].value()-h/2,dct[r][c].value()+h/2,"input",0,r,c);
    }
  quantization();
  IDCT();
//  for (r = 0; r < N; r++)
//    for (c = 0; c < N; c++)
//    SIADDMD (si1,idct[r][c],"intermediate",1,1);
  MSE_PSNR();
  SIOUTMD(si1, MSE, 1.0, "output", 2, 1);
  //SIOUTMD(si1, PSNR, 1.0, "output", 2, 1);

  si1.analyse();
  si1.write_to_file("output/significance.dat");
  //std::cout << si1 << std::endl;
  //sdct_out(si1);
  return 0;
}
