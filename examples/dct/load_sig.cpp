// SCoRPiO simple example 202  Binary input from file 

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

  using namespace std;

int main()
{

  //  intitialize interval library
  filib::fp_traits<double>::setup();

  // create a scorpio_info without internal tape creation
  scorpio_info si1("Data from file" );
  
   // read from file 
  si1.read_from_file("output/significance.dat");

  vector<double> sig(64,0);
  int k,l;
  for( int i = 0; i < si1.size(); ++i) {
    if(si1[i].tsk1() == 0){
      k = si1[i].tsk2()%8;
      l = si1[i].tsk3()%8;
      sig[k*8 + l] += si1[i].significance();
    }
  }

  ofstream ofs;
  ofs.open ("output/sig.txt");
  ofs << scientific;
  for(int i = 0; i < 8; i++){
    for(int j = 0; j < 8; j++){
     ofs << sig[i*8 + j] << " ";
    }
    ofs << "\n";
  }
  ofs.close();

  // free internal allocated memory
  si1.clear();  

}
