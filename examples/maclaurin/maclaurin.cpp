// taylor series for f(x) = 1/(1-x) for a = 0 ( Maclaurin series )
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <iostream>
#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

unsigned NUM_TERMS;
scorpio_info si1("taylor");

// compute one taylor coefficient 
//   representing individual tasks, input x, output t
template< typename T > 
void taylor_series_task(long pos, T &term, T x)
{
  T ret;
  ret = pow(x, pos);
  term = ret;
}

// taylor series for f(x) = 1/(1-x) for a = 0 ( Maclaurin series )
// input : n     number of factors (tasks)
//         cx    center of input range
//         rx    radius of input range
template< typename T > 
void taylor_series(double cx, double rx)
{
  T      *terms = new T[NUM_TERMS];
  T      x;
  T      ret;
  long   i;

  // register input variable in scorpio_info
  //   set input range
  SIINPMD(si1, x, cx-rx, cx+rx, "Input", 0 );

  // compute taylor coefficients
  for (i=0; i<NUM_TERMS; ++i )
    {
      std::cout << i << std::endl;
      /////  TASK STARTS HERE
      //  compute task output, store with task numbers 1,i
      //taylor_series_task(i, terms[i], x);
      terms[i] = pow(x, i);
      SIADDMD(si1, terms[i], "terms[i]",3,i);
      /////  TASK ENDS HERE
    }
	
  //// compute final output
  ret = 0;
  for (i=0; i<NUM_TERMS; ++i)
    {
      ret += terms[i];
    }

  //  register output variable,  prepare significance analysis
  SIOUTMD(si1, ret, 1.0, "final value", 1, 1 );
  // do significance analysis: propagate and store adjoints
  si1.analyse( );
  // write to file
  //dco::ia1s::print_iio();
  //dco::ia1s::write_dot();

  // clean up
  delete[] terms;
  
  // return midpoint of interval
  //return ret;

}  // taylor_series 


int main(int argc, char* argv[])
{
  double cx, rx;
  //filib::fp_traits<double>::setup();   //  initialise  interval library

  NUM_TERMS = 5; //atoi(argv[1]);
  cx        = 0.5; //atof(argv[2]);
  rx        = 0.2; //atof(argv[3]);
  dco::ia1s::type      *terms = new dco::ia1s::type[NUM_TERMS];
  dco::ia1s::type      x;
  dco::ia1s::type      ret;
  long   i;

  // register input variable in scorpio_info
  //   set input range
  SIINPMD(si1, x, cx-rx, cx+rx, "Input", 0 );

  // compute taylor coefficients
  for (i=0; i<NUM_TERMS; ++i )
    {
      /////  TASK STARTS HERE
      //  compute task output, store with task numbers 1,i
      //taylor_series_task(i, terms[i], x);
      terms[i] = pow(x, i);
      SIADDMD(si1, terms[i], "terms[i]",3,i);
      /////  TASK ENDS HERE
    }

  //// compute final output
  ret = 0;
  for (i=0; i<NUM_TERMS; ++i)
    {
      ret += terms[i];
    }

  //  register output variable,  prepare significance analysis
  SIOUTMD(si1, ret, 1.0, "final value", 1, 1 );
  // do significance analysis: propagate and store adjoints
  si1.analyse();
  si1.graph(0.35);
  si1.code(0,"output/f");
  si1.code(0.35,"output/g");
  // write to file
  si1.write_to_file("significance.dat");
  std::cout << si1 << std::endl;

  // clean up
  delete[] terms;

  return 0;
}
