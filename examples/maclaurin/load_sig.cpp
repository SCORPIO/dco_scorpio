// SCoRPiO simple example 202  Binary input from file 

#include <iostream>
#include <string>

#include "dco_scorpio.hpp"
#include "scorpio_info.hpp"

  

int main()
{

  //  intitialize interval library
  filib::fp_traits<double>::setup();

  // create a scorpio_info without internal tape creation
  scorpio_info si1("Data from file" );
  
   // read from file 
  si1.read_from_file("significance.dat");


  // print out all information stored in scorpio_info_stack si1
  std::cout << "\n--- SI1 std::cout  ------------\n";
  std::cout << si1;  
  std::cout << "\n-----------------------------\n";

  std::cout << " -------------------------------------------" << std::endl;
    
  // free internal allocated memory
  si1.clear();  

}
