\renewcommand{\docheader}{\dcos User Documentation }

\doctitle{Interval Adjoint Mode}
\label{ch:ia1s}

\section{Purpose}
The \lstinline{dco::ia1s} namespace implements the interval adjoint mode AD as outlined in Chapter~\ref{ch:intro}.
It provides the data type \lstinline{dco::a1s::type} and appropriately overloaded versions of the arithmetic operators and intrinsic functions in C++.
The overloaded arithmetic operators and intrinsic functions  generate an image of the computation stored as a directed acyclic graph~(DAG).
In this chapter we use bold face for continuous intervals, represented by two real bounds.
$$\X = [\lb{x}, \ub{x}] = \{\X \in \R|\lb{x} \leq x \leq \ub{x}\}$$
$\mathbb{IR}$ denotes the space of all finite intervals.
A given implementation of a multivariate interval vector function
\begin{equation}
\label{eqn:a1s_f}
\Y=F(\X) : \IR^n \rightarrow \IR^m
\end{equation}
is transformed into code for evaluating
\begin{equation}
\label{eqn:a1s_df}
\begin{split}
\X_{(1)}&:=\X_{(1)}+<\Y_{(1)},\nabla F(\X)> \\ 
\Y&:=F(\X) ,
\end{split}
\end{equation}
in which the subscript $_{(1)}$ denotes the adjoint of the corresponding variable.

\section{User Interface}
\subsection{Include Statements}
The user needs to include the following headers to enable the interval-adjoint significance analysis with \dcos. 
\begin{lstlisting}
  #include "dco_scorpio.hpp"
  #include "scorpio_info.hpp"
\end{lstlisting}
The first include statement makes the data type \lstinline{dco::ia1s::type} available.

The second header file includes the class \lstinline{scorpio_info} that stores the significance information of variables in objects of type \lstinline{scorpio_info_item}.

\subsection{\dcos Type}
To obtain interval values and interval adjoints for a function the data type of all floating point variables has to be changed to \lstinline{dco::ia1s::type}.
For this data type the following arithmetic operations and built-in functions are overloaded to generate a directed acyclic graph:
    
\begin{tabularx}{0.8\textwidth}{ |p{0.25\textwidth}|X| }
\hline
Unary Operators & $+$, $-$ \\
Binary Operators & $+$, $-$, $*$, $/$ \\
Assignment Operators & $=$, $+=$, $-=$, $*=$, $/=$ \\
Increment/Decrement & $++$, $--$ \\
Unary Intrinsic & abs, round, ceil, floor, sqrt, cos, sin, tan, acos, asin, atan, cosh, sinh, tanh, exp, log, log10 \\
Binary Intrinsic & pow, atan2, min, max\\
Comparison Operators & $==$, $!=$, $<$, $<=$, $>$, $>=$\\
\hline
\end{tabularx}

\subsection{Variable Declarations}
The interval-adjoint significance analysis requires at least one instance of class \lstinline{scorpio_info}.
\begin{lstlisting}
  scorpio_info si( [descr] );
\end{lstlisting}
The argument \lstinline{descr} has type \lstinline{char*} and is optional.

\subsection{Registration of Variables}
For the significance analysis the user needs to define input and output variables.
Additionally, intermediate variables can be registered to make their information available after the analysis.

\subsubsection{Input Variables}
To register input variables of the computation the macros provided by the \lstinline{scorpio_info} class can be used:

\begin{lstlisting}[]
  INPUT( si, var, lb, ub, descr, t1, [t2, ..., t6]);
\end{lstlisting}

\lstinline{INPUT} can be replaced by \lstinline{SIINPM} and \lstinline{SIINPMD} as described in \cite{del11}.

\begin{center}
\begin{tabularx}{0.85\textwidth}{l|l|l}
	Argument & Description & Data type\\
	\hline
	\lstinline{si} & Name of the \lstinline{scorpio_info} variable & \lstinline{scorpio_info}\\
	\lstinline{var} & Name of the input variable & \lstinline{dco::ia1s::type}\\
	\lstinline{lb} & Lower bound of input range & \lstinline{double}\\
	\lstinline{ub} & Upper bound of input range & \lstinline{double}\\
	\lstinline{descr} & Description & \lstinline{char*}\\
	\lstinline{t1} & Task specifier 1 (mandatory) & int\\
	\lstinline{t2, ..., t6} & Additional task specifiers (optional) & int
\end{tabularx}
\end{center}

\subsubsection{Intermediate Variables}
An intermediate variables can be any variable which is neither an input nor an output variable.
If the significance of an intermediate variable is of interest the following macros can be used to add those to the \lstinline{scorpio_info} object:

\begin{lstlisting}[]
  INTERMEDIATE( si, var, descr, t1, [t2, ..., t6]);
\end{lstlisting}

\lstinline{INTERMEDIATE} can be replaced by \lstinline{SIADDM} and \lstinline{SIADDMD} as described in \cite{del11}.

\begin{center}
\begin{tabularx}{0.85\textwidth}{l|l|l}
	Argument & Description & Data type\\
	\hline
	\lstinline{si} & Name of the \lstinline{scorpio_info} variable & \lstinline{scorpio_info}\\
	\lstinline{var} & Name of the input variable & \lstinline{dco::ia1s::type}\\
	\lstinline{descr} & Description & \lstinline{char*}\\
	\lstinline{t1} & Task specifier 1 (mandatory) & int\\
	\lstinline{t2, ..., t6} & Additional task specifiers (optional) & int
\end{tabularx}
\end{center}

\subsubsection{Output Variables}
The output variables are those variables that are computed by the program.
To register output variables the following macros can be used:

\begin{lstlisting}[]
  OUTPUT( si, var, adj, descr, t1, [t2, ..., t6]);
\end{lstlisting}

\lstinline{OUTPUT} can be replaced by \lstinline{SIOUTM} and \lstinline{SIOUTMD} as described in \cite{del11}.

\begin{center}
\begin{tabularx}{0.85\textwidth}{l|l|l}
	Argument & Description & Data type\\
	\hline
	\lstinline{si} & Name of the \lstinline{scorpio_info} variable & \lstinline{scorpio_info}\\
	\lstinline{var} & Name of the input variable & \lstinline{dco::ia1s::type}\\
	\lstinline{adj} & Adjoint value & \lstinline{double}\\
	\lstinline{descr} & Description & \lstinline{char*}\\
	\lstinline{t1} & Task specifier 1 (mandatory) & int\\
	\lstinline{t2, ..., t6} & Additional task specifiers (optional) & int
\end{tabularx}
\end{center}

\subsection{Start the Analysis}
To compute the interval values, the interval adjoints and the significance of an instance of the \lstinline{scorpio_info} class \lstinline{si} the following function call can be used:
\begin{lstlisting}
  si.analyse();
\end{lstlisting}
After this call the significance information for all registered variables is available in the \lstinline{scorpio_info} instance.

\subsection{Reset the Significance Information}
Individual significance analysis runs for every output can be done by a single \lstinline{scorpio_info} instance.
For that, the instance \lstinline{si} needs to be reset by
\begin{lstlisting}
  si.reset();
\end{lstlisting}

\section{Access the Significance Results}
\subsection{Access to information of \lstinline{scorpio_info}}
\lstinline{scorpio_info} was designed as a container class for significance information in the tradition of container classes of the STL (standard template library).
Usually, the following methods of \lstinline{scorpio_info} are used only:

\begin{center}
\begin{tabularx}{\textwidth}{l|l}
	Method signature & Description\\
	\hline
	\lstinline{scorpio_info_item operator[] (int n)} & Returns a copy of the \lstinline{n}-th entry\\
	\lstinline{int size()} & Number of \lstinline{scorpio_info_item} stored\\
	\lstinline{void clear()} & Free memory used internally
\end{tabularx}
\end{center}

Nevertheless, other methods of the STL container class interface have been implemented too:

\begin{center}
\begin{tabularx}{0.9\textwidth}{l}
  \lstinline{void push_back(const scorpio_info_item& si)} \\
  \lstinline{int capacity()} \\
  \lstinline{void reserve()} \\
  \lstinline{void resize(int n)} \\
  \lstinline{std::vector<scorpio_info_item>::iterator begin()} \\
  \lstinline{std::vector<scorpio_info_item>::iterator end()} \\
  \lstinline{std::vector<scorpio_info_item>::reverse_iterator rbegin()} \\
  \lstinline{std::vector<scorpio_info_item>::reverse_iterator rend()} \\
  \lstinline{std::ostream& operator<< (std::ostream &s, scorpio_info &si)}
\end{tabularx}
\end{center}

\subsection{Access to information of \lstinline{scorpio_info_item}}
The following methods of \lstinline{scorpio_info_item} can be used to retrieve the significance information from significance entries:
\begin{center}
\begin{tabularx}{\textwidth}{l|l}
	Method signature & Description\\
	\hline
	\lstinline{double significance()} & significance value, default criteria \\ 
	\lstinline{double significance(int i)} & significance value, \lstinline{i}-th criteria \\ 
	\lstinline{interval value()} & interval value \\ 
	\lstinline{interval adjoint()} & interval adjoint \\ 
	\lstinline{std::string name()} & name and description \\ 
	\lstinline{std::string file()} & file name of registration \\ 
	\lstinline{int line()} & line number of registration \\ 
	\lstinline{int tsk1()} & 1st task identifier \\ 
	\lstinline{int tsk2()} & 2nd task identifier \\ 
	\lstinline{int tsk3()} & 3rd task identifier \\ 
	\lstinline{int tsk4()} & 4th task identifier \\ 
	\lstinline{int tsk5()} & 5th task identifier \\ 
	\lstinline{int tsk6()} & 6th task identifier \\ 
	\multicolumn{2}{l}{\lstinline{std::ostream& operator<< (std::ostream &s, scorpio_info_item &sii)}}
\end{tabularx}
\end{center}

\subsection{Read and Write File of the Significance Analysis}
To store the results of the significance analysis in an external file the method
\begin{lstlisting}
  si.write_to_file(std::string fname);
\end{lstlisting}
can be used, where \lstinline{si} is an instance of class \lstinline{scorpio_info} and \lstinline{fname} denotes the file name.

Similar the external file can be loaded by
\begin{lstlisting}
  si.read_from_file(std::string fname);
\end{lstlisting}
where \lstinline{si} is an instance of class \lstinline{scorpio_info} and \lstinline{fname} denotes the file name.

\section{Code Flattener}
Due to the fact that comparison operators are not implemented in the underlying IA library \texttt{FILIB++} we simplify the code for the analysis.
The single assignment code as well as the computational graph are created for the a specified floating-point computation.
The user specifies the decisions by the floating-point values that are assigned to the active variables.
This is illustrated by \texttt{simple4.cpp} in the example folder.

\section{Graph Visualization}
The user can generate an output DOT file that can be used for a visualization of the computational graph.
For that, the user has to use the method
\begin{lstlisting}
  si.graph([double eps], [std::string fname]);
\end{lstlisting}
where \lstinline{si} is an instance of class \lstinline{scorpio_info}, \lstinline{eps} denotes the significance bound and \lstinline{fname} is the filename.
If no significance bound is given the default value is zero.
All variables that have a higher significance value compared to the bound are significant.
All other variables are insignificant and visualized with a red framed node.
The default output filename is \emph{graph.dot}.

To render the DOT graph one can use for example Graphviz\footnote{\url{http://www.graphviz.org/}} with the following command:
\begin{lstlisting}
  dot -s2 -Tpdf graph.dot -o graph.pdf
\end{lstlisting}
This method is only available after the analysis.

\section{Code Generation}
By assuming that each insignificant computation can be replaced by a constant the following method transforms the computational graph into single assignment source code:
\begin{lstlisting}
  si.code([double eps], [std::string fname]);
\end{lstlisting}
where \lstinline{si} is an instance of class \lstinline{scorpio_info}, \lstinline{eps} denotes the significance bound and \lstinline{fname} is the filename.
The default value for \lstinline{eps} is zero and the default filenames are \emph{f.h} and \emph{f.cpp}.
This method is only available after the analysis.

\section{Interval Splitting}
The general idea of interval splitting is to divide value intervals of intermediate variables $[v_i]$ into a predefined number of subinterval and generate multiple scenarios.
Thus, IASA becomes applicable to computer programs with arbitrary control flow.
For a comparison of an interval with a constant $[v_i] < c$ with $c\in[v_i]$ the interval is split at that constant and two scenarios are generated:
One with $[\underline{v_i},c)$ and another one with $[c,\overline{v_i}]$.

We consider two variants of application of interval splitting, the exploration mode and the quantification mode.
The exploration mode is a sort of black-box analysis, in which input variables are split, especially those variables with large value or adjoint intervals.
On the other side, in the quantification mode the splitting will just be applied to user selected input variables to find input domains that are promising in terms of computational savings.
The quantification mode can also validate intuitive or well known characteristics of a code.
Both modes requires the user to give an upper bound for the maximal allowed width of all value and adjoint intervals.

Interval splitting of the input domain yields the application of IASA to a subset $D_k \subseteq D$ of the original domain.
A scenario $s_k = (G_k,D_k)$ is uniquely defined by its input domain $D_k$ and the corresponding DAG $G_k$.
IASA is applied recursively to new scenarios.
%The significance analysis has to be reapplied to all scenarios $s_k$ and each run of the analysis can recursively generate more scenarios.
If the analysis generates new scenarios the analysis has to be reapplied to all new scenarios which itself might generate more scenarios.
This procedure potentially results in a very large number of scenarios which can involve high computational costs.

To apply the interval splitting in quantification mode the user has to use the shell script \lstinline{iasa.sh} that is supplied by the software after the compilation.
In the source code the user has to define the splitting variables by
\begin{lstlisting}
  si.register_split(dco::ia1s::type v, double beta, double gamma);
\end{lstlisting}
In this function call \lstinline{beta} is an upper bound for the width of the value interval and \lstinline{gamma} an upper bound for the width of the adjoint interval, such that variable $v$ is only split if $w(\V) > \beta$ or $w(\V_{(1)}) > \gamma$.
Furthermore, the \lstinline{analyse} method changes and now requires two arguments
\begin{lstlisting}
  si.analyse(int sid, double eps);
\end{lstlisting}
The first argument is the scenario ID and the second argument is a bound used for the interval splitting.
Both arguments are command-line argument and assigned via the script \lstinline{iasa.sh}.

Furthermore, output files for this analysis and the individual scenarios are stored in the following paths:
\begin{center}
\begin{tabularx}{0.9\textwidth}{l}
  \lstinline{data/analyse} \\
  \lstinline{data/finish} \\
  \lstinline{data/rank} \\
  \lstinline{data/graph} \\
  \lstinline{data/code} \\
\end{tabularx}
\end{center}

In the examples the heat conduction benchmark applies interval splitting.

The control flow splitting as well as the exploration mode are not implemented in the current version.
